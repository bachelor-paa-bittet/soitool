# Repo for bachelor SOI-tool

SOI-tool er et verktøy for å lage SOI - "Samband- og operasjonsinstruks". Programvaren ble utviklet som en bacheloroppgave av fire studenter ved NTNU våren 2020 med Opplandske Heimevernsdistrikt 05 som oppdragsgiver. Dette repoet inneholder all kildekode for oppgaven. Detaljer rundt implementasjonen finnes i tilhørende bachelorrapport.  

## Gruppen

| Gruppemedlem     | LinkedIn                                      | GitHub              | Nettside  |
|------------------|-----------------------------------------------|---------------------|-----------|
| Thomas Løkkeborg | www.linkedin.com/in/tholok                    | github.com/tholok97 | tholok.io |
| Nikolai Mork     | www.linkedin.com/in/nikolai-mork              | github.com/morkolai |           |
| Anders H. Rebner | www.linkedin.com/in/anders-h-rebner-59b60219b |                     | rebner.me |
| Petter Sagvold   |                                               |                     |           |

## Forutsetninger

* `Python 3.7`
* Windows 10 eller Ubuntu 18.04. Merk at flesteparten av gruppen arbeider på Windows 10.

## Hvordan kjøre programvaren

Når oppsett under er fullført kan programvaren startes med `python -m soitool.main_window` kjørt fra roten av repository. Merk at programvaren **må** kjøres på denne måten.

## Oppsett med virtual environment

```bash
# initialiser virtual environment
python -m venv venv
# aktiver virtual environment (Windows)
./venv/Scripts/activate
# aktiver virtual environment (Ubuntu, ikke testet)
./venv/bin/activate
# installer pakker
pip install -r requirements.txt
# ...jobb med koden...
# for å deaktivere virtual environment
deactivate
```

### Om prosedyre over foråsaker problemer

Følgende var en del av prosedyren over før, men ble fjernet da det viste seg ikke å være nødvendig. Om prosedyren over gir problemer kan det hende følgende kan hjelpe. Dette må kjøres **inne** i virtual environment.

```bash
# sikre at venv har riktige versjoner av kritiske innebygde pakker
# (kanskje unødvendig)
# dette blir gjort fordi default gir pip-18, som gir errormeldinger.
# Se https://bugs.python.org/issue30628
python -m pip install pip=='20.0' setuptools=='45.2' wheel=='0.34'
```

## Linting

Sjekk av kodekvalitet gjøres med scriptet "CodeQualityCheck" i script-mappa, og er skrevet i Powershell(.ps1) og Bash(.sh). Scriptet kjører Pylint og Flake8 med tilhørende konfigurasjonsfiler, Pydocstyle med numpy-konvensjon og Bandit på Python-filer. 

Scriptet godtar kommandolinjeargumenter: .py-fil(er), mappe(r) eller en blanding av disse. Uten argumenter vil scriptet sjekke alle .py-filer.

Terminal kjøres fra root. Sjekk:

* Alle .py-filer: `.\scripts\CodeQualityCheck.ps1` eller `./scripts/CodeQualityCheck.sh`

* Spesifikk(e) .py: `.\scripts\CodeQualityCheck.ps1 filEn.py filTo.py`

* Alle .py i mappe(r): `.\scripts\CodeQualityCheck.ps1 mappeEn mappeTo`

* Blanding: `.\scripts\CodeQualityCheck.ps1 mappeEn mappeTo\fil.py`

## Testing

Tester er skrevet med `unittest` modulen, og ligger i mappen `test`.

* For å kjøre alle tester: `python -m unittest`
* For å kjøre en enkelt test modul (fil i `test` mappen): `python -m unittest test.<navnet_til_modulen>`
* For å kjøre en enkelt test metode (fil i `test` mappen): `python -m unittest test.<navnet_til_modulen>.<navnet til klassen>.<navnet til metoden>`

## Generering av kodedokumentasjon

Gjøres med pdoc3:

* `pdoc3 --html --output-dir .\docs .\soitool\main.py`

* Uten kildekode: `pdoc3 --html --config show_source_code=False --output-dir .\docs\ .\soitool\main.py`

Autogenerert docs: http://bachelor-paa-bittet.pages.stud.idi.ntnu.no/soitool/

## Om `Dockerfile`

Docker image som brukes i `.gitlab-ci.yml` er bygget med filen `Dockerfile` og er lastet opp som `morkolai/soitool-ci`. Docker image inneholder alle avhengigheter til prosjektet. Følgende prosedyre brukes for å oppdatere image. Dette må gjøres når `requirements.txt` endrer seg.

```bash
docker build -t morkolai/soitool-ci .
docker login
docker push morkolai/soitool-ci
```

## Pakking av leverbar ZIP

Programvare leveres i en ZIP bestående av følgende filer og mapper:

```text
SOI-tool/                    Mappe med .exe fil klar for bruk
Installasjonsinstrukser.txt  Instasllasjonsinstrukser
Kildekode/                   All kildekode
Dokumentasjon/               HTML dokumentasjon generert med pdoc3
```

Følgende underseksjoner går i detaljer på hvordan hver enkelt av disse skal forberedes. Når alle komponenter er klare skal disse komprimeres til en ZIP-fil. I Windows kan dette gjøres ved å velge dem i filutforsker, trykke "del", og deretter trykke "ZIP". Navnet på ZIP-fil burde endres til noe sånt som "SOI-tool.zip". Filen er da klar for leveranse, og det forventes at mottaker forstår hvordan innholdet skal brukes ved å lese installasjonsinstruksene.

### Mappe med `.exe` fil

Ved å kjøre kommando under ender vi opp med en mappe `dist/SOI-tool` som inneholder applikasjonen. Denne mappen skal inneholde alt applikasjonen vår trenger for å kjøre. Applikasjonen kan startes ved å kjøre `SOI-tool.exe`. Database vil bli lagret i `soitool/database`. Filer generert av applikasjonen vil ende opp i mappen brukeren åpnet applikasjonen fra. `SOI-tool/` mappen kan zippes opp og sendes til sluttbrukere. En snarvei kan bli laget til `.exe` filen inne i mappen for å gjøre applikasjonen mer brukervennlig.

Om ønskelig kan `--windowed` legges til på kommandoen under for å generere en `.exe` uten konsoll med stdout output.

```shell
python -m PyInstaller --add-data 'soitool/modules/config;soitool/modules/config' --add-data 'soitool/media;soitool/media' --add-data 'soitool/testdata;soitool/testdata' --name "SOI-tool" .\soitool\main_window.py
```

Inne i mappen `dist/` ender man da opp med mappen `SOI-tool/` som alt vi trenger for å kjøre programvaren. Det er denne mappen som skal inkluderes i ZIP.

### Installasjonsinstrukser

Bruk filen `Installasjonsinstrukser.txt`.

### Mappe med `pdoc3` dokumentasjon

```shell
pdoc3 soitool --html --output-dir htmldocs --force
```

Inne i mappen `htmldocs` ender man da opp med en mappe `soitool`. Det er denne mappen som skal inkluderes i ZIP. Gi mappen nytt navn som beskrevet på starten av "Pakking av leverbar ZIP"

### Mappe med kildekode

Bruk en kopi av hele repository, eksludert alle filer som `.gitignore` ignorerer. Aller helst burde mappen komme rett fra `git clone` av vårt repository. Gi mappen nytt navn som beskrevet på starten av "Pakking av leverbar ZIP"

## Arbeidsmetode

### Bruk av `git`

Arbeid skal ikke skje direkte på `master` branch. For hver oppgave en vil utføre skal en ny branch lages, og denne må senere merges inn ved hjelp av en "Merge Request". Gjennomgang av dette er lagt fram under:

```bash
# ny branch
git branch <branch navn>
# hoppe til eksisterende branch
git checkout <branch navn>
# ..jobb med koden..
git add <...>
git commit -m "..."
# push til gitlab
git push origin <branch navn>
```

Merging til master skal skje via Merge Requests i GitLab.

#### Om arbeid utføres på feil branch

`git stash` kan brukes for å lagre endringer i et "stash". Deretter kan en hoppe til riktig branch med `git checkout <branch navn>`, og kjøre `git stash pop`.

#### Ved feil commit melding

Om det er siste commit som er problemet, og den ikke har blitt pushet opp, kan en gjøre følgende for å endre commit-melding

```
git commit --amend
```

Om en vil endre tidligere commits eller endringen har blitt pushet opp: rop om hjelp!

> "When in danger or in doubt, run in circles, scream and shout"

### Hvordan skrive tester

Hver modul burde testes. I praksis vil dette si at hver fil under `soitool/` med navn `X.py` burde ha en tilsvarende fil under `test/` med navn `test_X.py`.
