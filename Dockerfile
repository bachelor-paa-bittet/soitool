FROM ubuntu:18.04

# This Dockerfile describes the container used in .gitlab-ci.yml

# Need docker build arg, such as --build-arg FLAKE8_VERSION=3.0.4
ARG FLAKE8_VERSION=3.7

# Installing python3.7
RUN apt-get update
RUN apt-get install software-properties-common -y
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get update
RUN apt-get install python3.7 -y
RUN python3.7 -V
RUN apt-get install python3-pip -y
RUN pip3 install --upgrade pip

# To make -platform offscreen,minimal,vnc work
RUN apt-get install -y libegl1-mesa-dev \
                       libfontconfig1-dev \
                       libsdl1.2-dev \
                       libwayland-dev \
                       libxkbcommon-dev

WORKDIR /code
COPY requirements.txt requirements.txt
RUN python3.7 -m pip install setuptools
RUN python3.7 -m pip install -r requirements.txt


# Set the locale
# Necessary for container to properly handle æøå
# See issue #10
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8


# From https://it.i88.ca/2016/03/how-to-fix-importerror-libgssapikrb5so2.html
# python3.7 -m unittest fails without this
RUN apt-get -y install libgssapi-krb5-2

# Necessary for import of PySide2.QtWebEngineWidgets to work
RUN apt-get -y install libnss3 \
                       libxcomposite-dev \
                       libxcursor-dev \
                       libxi-dev \
                       libxtst-dev \
                       libxrandr-dev
