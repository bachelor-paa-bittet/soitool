"""Includes two help-popups.

Has two dialogs that appear when actions from help are triggered.
"""
from PySide2.QtWidgets import (
    QDialog,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QFormLayout,
    QScrollArea,
    QWidget,
)
from PySide2.QtCore import Qt
from PySide2.QtGui import QFont


class ShortcutsHelpDialog(QDialog):
    """The dialog for hotkeys.

    This class contains the information for the shortcuts/hotkeys that
    can be used in the application
    """

    def __init__(self):  # pylint: disable = R0915
        super().__init__()
        self.resize(300, 300)
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)

        self.layout = QVBoxLayout()
        self.layout_label = QFormLayout()
        self.layout_button = QHBoxLayout()

        self.button_ok = QPushButton("Ok")
        self.button_ok.clicked.connect(self.close)

        self.header_main = QLabel("Hurtigtaster for applikasjonen:")
        self.header_main.setFont(QFont("Verdana", 10))
        self.header_table = QLabel("Hurtigtaster for redigerbare tabeller:")
        self.header_table.setFont(QFont("Verdana", 10))
        self.header_modules = QLabel("Hurtigtaster for noen gitte moduler:")
        self.header_modules.setFont(QFont("Verdana", 10))

        # Main/program shortcuts
        self.layout_label.addRow(self.header_main)
        self.layout_label.addRow(
            QLabel("Opprett en ny SOI: "), QLabel("Ctrl + N")
        )
        self.layout_label.addRow(
            QLabel("Åpne SOI fra fil: "), QLabel("Ctrl + O")
        )
        self.layout_label.addRow(
            QLabel("Åpne SOI fra database: "), QLabel("Ctrl + D")
        )
        self.layout_label.addRow(
            QLabel("Endre oppsett SOI: "), QLabel("Ctrl + I")
        )
        self.layout_label.addRow(
            QLabel("Legg til ny modul: "), QLabel("Ctrl + M")
        )
        self.layout_label.addRow(
            QLabel("Reorganiser moduler: "), QLabel("Ctrl + Mellomrom")
        )
        self.layout_label.addRow(QLabel("Eksporter PDF: "), QLabel("Ctrl + P"))
        self.layout_label.addRow(
            QLabel("Eksporter komprimert SOI: "), QLabel("Ctrl + E")
        )
        self.layout_label.addRow(
            QLabel("Lagre i database: "), QLabel("Ctrl + S")
        )
        self.layout_label.addRow(
            QLabel("Zoom inn: "), QLabel("Ctrl + Scroll oppover")
        )
        self.layout_label.addRow(
            QLabel("Zoom ut: "), QLabel("Ctrl + Scroll nedover")
        )

        # Table shortcuts
        self.layout_label.addRow(QLabel())
        self.layout_label.addRow(self.header_table)
        self.layout_label.addRow(QLabel("Legg til rad"), QLabel("Ctrl + +"))
        self.layout_label.addRow(QLabel("Fjern rad"), QLabel("Ctrl + -"))
        self.layout_label.addRow(
            QLabel("Legg til kolonne"), QLabel("Shift + +")
        )
        self.layout_label.addRow(QLabel("Fjern kolonne"), QLabel("Shift + -"))

        # Other module-specific shortcuts
        self.layout_label.addRow(QLabel())
        self.layout_label.addRow(self.header_modules)
        self.layout_label.addRow(
            QLabel("Rediger forhåndsavtalte koder: "), QLabel("Ctrl + R")
        )
        self.layout_label.addRow(
            QLabel("Rediger telefonliste: "), QLabel("Ctrl + R")
        )
        self.layout_label.addRow(
            QLabel("Rediger frekvenstabell: "), QLabel("Ctrl + R")
        )

        self.layout_button.addWidget(self.button_ok, alignment=Qt.AlignRight)

        self.layout.addLayout(self.layout_label)
        self.layout.addLayout(self.layout_button)
        self.setLayout(self.layout)


class BasicUsageHelpDialog(QDialog):
    """The dialog for easy use.

    This class contains text that explains the user how to use the application.
    """

    def __init__(self):  # pylint: disable = R0915
        super().__init__()
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)

        self.resize(900, 600)

        headline = QLabel("Enkel bruk")
        headline.setStyleSheet(
            "font-weight: bold; font-size: 14pt; text-decoration: underline"
        )

        main_text = (
            "Programmets hovedfunksjonalitet ligger i toppmeny-alternativene "
            "'SOI' og 'Kodebok', og i åpne faner.\n\n"
            "Gjennom topp-menyen 'SOI' kan man lage ny SOI eller åpne en "
            "eksisterende SOI fra fil eller\ndatabase, disse åpnes i en egen "
            "fane. Det er også mulig å lagre SOI i database, i tillegg til å "
            "\neksportere SOI til en komprimert/ukomprimert serialisert fil, "
            "eller til PDF som egner seg for utskrift.\n\n"
            "Meny-alternativene som omhandler lagring og eksport gjelder for "
            "den aktive fanen.\n"
            "Gjennom topp-menyen 'Kodebok' kan man åpne en fane for å se "
            "og/eller redigere kodeboka, \noppdatere kodeboka med nye koder, "
            "eller eksportere stor/liten kodebok som PDF."
        )
        main_label = QLabel(main_text)
        main_label.setStyleSheet("font-size: 10pt")

        soi_edit_headline = QLabel("Rediger SOI")
        soi_edit_headline.setStyleSheet("font-weight: bold; font-size: 11pt")
        soi_edit_text = (
            "Når en SOI opprettes, har den standard verdier for bl.a. "
            "overskrift, beskrivelse, gradering og datoer.\nDisse kan endres "
            "gjennom 'Oppsett'-knappen i nedre, venstre hjørne.\n\n"
            "Moduler legges til gjennom 'Ny modul'-knappen i nedre, venstre "
            "hjørne. Da åpnes en dialog der man kan\nvelge hvilken modul man "
            "vil ha, og man kan velge modulnavn og om modulen skal være et "
            "vedlegg.\n"
            "Vedleggsmoduler havner på en egen side. Liste over eksisterende "
            "moduler og vedlegg finnes på venstre\nside.\n\n"
            "Når en modul er lagt til, kan den redigeres direkte. Noen "
            "moduler har også hurtigtaster\n(se hjelp -> hurtigtaster) som "
            "kan brukes etter at modulen er klikket på.\n\n"
            "Etter en modul har blitt redigert kan den ende opp med å "
            "overlappe andre moduler. For å reorganisere\nmodulene kan en "
            "enten trykke på 'Reorganiser moduler'-knappen i nedre, venstre "
            "hjørne, eller trykke\npå selve SOI arket.\n\n"
            "En kan zoome ut og inn i SOI ved å holde nede Ctrl og scrolle "
            "med musehjulet."
        )
        soi_edit_label = QLabel(soi_edit_text)
        soi_edit_label.setStyleSheet("font-size: 10pt")

        soi_module_placement_headline = QLabel("Modulplassering")
        soi_module_placement_headline.setStyleSheet(
            "font-weight: bold; font-size: 10pt"
        )
        soi_module_placement_text = (
            "Under 'oppsett' kan man velge mellom to strategier for "
            "modulplassering: manuell og automatisk.\n\n"
            "Manuell plassering er desverre ikke klart for bruk. "
            "Automatisk plassering innebærer at programvaren\nselv bestemmer "
            "hvordan moduler plasseres i SOI. Dette fører til mindre arbeid "
            "for SOI-forfatter, og\nvil ofte resultere i mer optimale "
            "plasseringer.\n\n"
            "Det er tre måter man kan påvirke den automatiske "
            "modulplasseringen: Sidevalg, plasseringsalgoritme og\n"
            "forhåndssortering. Med sidevalg kan man påvirke hvilken side "
            "moduler skal plasseres på. Med\nplasseringsalgoritme kan man "
            "påvirke hvordan moduler plasseres innad i en side. Standard "
            "algoritme er\nofte et godt valg, men man kan eksperimentere med "
            "ulike algoritmer for å oppnå nye plasseringer. Med\n"
            "forhåndssortering kan man påvirke rekkefølgen moduler plasseres "
            "i SOI. Man kan enten velge å la\nprogramvaren velge sortering, "
            "eller å styre sortering selv. Om man velger å sortere selv, vil\n"
            "rekkefølgen i modullisten til venstre respekteres. Dra-og-slipp "
            "for å omprioritere moduler i\nmodullisten."
        )
        soi_module_placement_label = QLabel(soi_module_placement_text)
        soi_module_placement_label.setStyleSheet("font-size: 10pt")

        codebook_headline = QLabel("Se/rediger kodebok")
        codebook_headline.setStyleSheet("font-weight: bold; font-size: 11pt")
        codebook_text = (
            "Når man har valgt 'Se/rediger kodebok' i kodebok-menyen, åpnes "
            "det en kodebok-fane. Øverst kan man\nlegge til ord/uttrykk "
            "gjennom innfylling og bruk av 'Legg til'-knappen.\n\n"
            "Under er en tabell som viser kodebokas innhold. Tabellen kan "
            "sorteres ved å klikke på kolonnenavnene.\nMan kan redigere "
            "innholdet ved å dobbelklikke på celler, dette gjelder alle "
            "kolonner med unntak av\nkode-kolonnen. Verdien i type-kolonnen "
            "er nødt til å være 'Liten' eller 'Stor', andre ord enn disse\n"
            "avvises.\n\n"
            "Ved å klikke på en rad i kodebok-tabellen og trykke 'delete' på "
            "tastaturet, sletter man den valgte raden."
        )
        codebook_label = QLabel(codebook_text)
        codebook_label.setStyleSheet("font-size: 10pt")

        self.layout = QVBoxLayout()
        self.layout_text = QVBoxLayout()
        self.layout_button = QHBoxLayout()

        self.button_ok = QPushButton("Ok")
        self.button_ok.clicked.connect(self.close)

        self.layout_text.addWidget(headline, alignment=Qt.AlignHCenter)
        self.layout_text.addWidget(main_label, alignment=Qt.AlignLeft)
        self.layout_text.addWidget(
            soi_edit_headline, alignment=Qt.AlignHCenter
        )
        self.layout_text.addWidget(soi_edit_label, alignment=Qt.AlignLeft)
        self.layout_text.addWidget(
            soi_module_placement_headline, alignment=Qt.AlignHCenter
        )
        self.layout_text.addWidget(
            soi_module_placement_label, alignment=Qt.AlignLeft
        )
        self.layout_text.addWidget(
            codebook_headline, alignment=Qt.AlignHCenter
        )
        self.layout_text.addWidget(codebook_label, alignment=Qt.AlignLeft)
        self.layout_button.addWidget(self.button_ok, alignment=Qt.AlignRight)

        self.layout.addLayout(self.layout_text)

        self.widget_container = QWidget()
        self.widget_container.setLayout(self.layout)

        self.scroll_area = QScrollArea()
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scroll_area.setWidget(self.widget_container)

        self.layout_container = QVBoxLayout()
        self.layout_container.addWidget(self.scroll_area)
        self.layout_container.addLayout(self.layout_button)

        self.setLayout(self.layout_container)
