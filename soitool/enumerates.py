"""Enumerates used by several files."""
from enum import Enum


class ModuleType(Enum):
    """Enumerate with types of modules."""

    MAIN_MODULE = 0
    ATTACHMENT_MODULE = 1


class CodebookSort(Enum):
    """Enumerate with ways to sort codebook."""

    WORD = 0
    CODE = 1
