"""Provides a dialog where the user can select new module to be added."""

from PySide2.QtWidgets import (
    QHBoxLayout,
    QVBoxLayout,
    QListWidget,
    QLineEdit,
    QListWidgetItem,
    QLabel,
    QCheckBox,
)
from PySide2.QtCore import QSize, Qt
from soitool.modules.module_table import TableModule
from soitool.modules.module_authentication_board import (
    AuthenticationBoardModule,
)
from soitool.modules.module_subtractorcodes import SubtractorcodesModule
from soitool.modules.module_freetext import FreeTextModule
from soitool.modules.module_frequency_table import FrequencyTableModule
from soitool.modules.module_phonebook import PhonebookModule
from soitool.modules.module_code_phrase import CodePhraseModule
from soitool.modules.module_predefined_codes import PredefinedCodesModule
from soitool.accept_reject_dialog import AcceptRejectDialog


# Constant holding all modules the user can choose from. This is intended as a
# point of extensibility. Further modules that are developed can simply be
# placed here, and the rest of the program will respect them.
MODULE_CHOICES = [
    TableModule,
    AuthenticationBoardModule,
    SubtractorcodesModule,
    FreeTextModule,
    FrequencyTableModule,
    PhonebookModule,
    CodePhraseModule,
    PredefinedCodesModule,
]


class NewModuleDialogListItem(QListWidgetItem):
    """Custom list item that includes a widget implementation of a module.

    Parameters
    ----------
    widget : subclass of ModuleBase
        The widget that implements the module.
    """

    def __init__(self, widget):
        super().__init__(widget.get_icon(), widget.get_user_friendly_name())
        self.widget_implementation = widget


class NewModuleDialog(AcceptRejectDialog):
    """Dialog to let user select module to be added to the SOI.

    When the dialog is closed the user's choice is fetched by directly
    inspecting the child widgets, for example: `self.list_module_choices` and
    `self.line_edit_name`. The dialog is otherwise used excactly as it's
    superclass QDialog: https://doc.qt.io/qt-5/qdialog.html

    Parameters
    ----------
    module_choices : list of subclasses of ModuleBase
        List of modules the user can choose from.
    """

    def __init__(self, module_choices=None):
        super().__init__()
        self.setWindowTitle("Ny modul")

        if module_choices is None:
            module_choices = MODULE_CHOICES

        self.list_module_choices = QListWidget()
        self.list_module_choices.setViewMode(QListWidget.IconMode)
        self.list_module_choices.setIconSize(QSize(125, 125))
        self.list_module_choices.setResizeMode(QListWidget.Adjust)
        self.list_module_choices.setMinimumSize(430, 400)

        for i, module_choice in enumerate(module_choices):
            self.list_module_choices.insertItem(
                i, NewModuleDialogListItem(module_choice),
            )

        # Set the first element as the default choice, whatever it is
        # Only relevant if there is at least one item
        if self.list_module_choices.count() > 0:
            self.list_module_choices.setCurrentItem(
                self.list_module_choices.item(0)
            )

        self.line_edit_name = QLineEdit()
        self.line_edit_name.setPlaceholderText(
            "La stå tom for automatisk navn"
        )
        self.label_name = QLabel("Navn:")

        self.layout_name = QHBoxLayout()
        self.layout_name.addWidget(self.label_name)
        self.layout_name.addWidget(self.line_edit_name)

        self.label_attachment = QLabel("Vedlegg:")

        self.checkbox_attachment = QCheckBox()

        self.layout_attachment = QHBoxLayout()
        self.layout_attachment.setAlignment(Qt.AlignLeft)
        self.layout_attachment.addWidget(self.label_attachment)
        self.layout_attachment.addWidget(self.checkbox_attachment)

        self.layout_wrapper = QVBoxLayout()
        self.layout_wrapper.addWidget(self.list_module_choices)
        self.layout_wrapper.addLayout(self.layout_name)
        self.layout_wrapper.addLayout(self.layout_attachment)

        self.layout_content.addLayout(self.layout_wrapper)
