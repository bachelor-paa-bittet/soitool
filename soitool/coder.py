"""Generate codes.

Source: # https://realpython.com/lessons/cryptographically-secure-random-data-python/
"""
import string
import secrets


def get_code(code_length, mode="ascii", space_interval=0, space_amount=1):
    """Generate a single random code.

    Parameters
    ----------
    code_length : int
        The length of the code.
    mode : string
        'ascii' for letters, 'digits' for digits and 'combo' for
        combination of letters and digits, by default 'ascii'.
    space_interval : int
        Spaces will be inserted into code each interval for readability if not
        0, by default 0.
    space_amount : int
        Amount of spaces per interval, by default 1.

    Return
    ------
    code : string
        The code.

    Raises
    ------
    ValueError
        If parameter mode is neither of the accepted.
    """
    code = ""

    if mode == "ascii":
        characters = string.ascii_uppercase
    elif mode == "digits":
        characters = string.digits
    elif mode == "combo":
        characters = string.ascii_uppercase + string.digits
    else:
        raise ValueError(
            "Invalid value for argument 'mode': " "'{}'".format(mode)
        )

    i = 0
    while i < code_length:
        letter = secrets.choice(characters)
        code += letter
        i += 1

    # Add spaces to code if interval is given
    if space_interval > 0:
        code = insert_spaces(code, space_interval, space_amount)

    return code


def get_code_set(
    count, code_length, mode="ascii", space_interval=0, space_amount=1
):
    """Generate a set of unique, random codes.

    Parameters
    ----------
    count : int
        Number of codes to be returned
    code_length : int
        The length of each code
    mode : string
        'ascii' for letters (default), 'digits' for digits and 'combo'
        for combination of letters and digits.
    space_interval : int
        Spaces will be inserted into code each interval for readability if not
        0, by default 0.
    space_amount : int
        Amount of spaces per interval, by default 1.

    Return
    ------
    codes : set
        Set of unique codes
    """
    codes = set()

    while len(codes) < count:
        code = get_code(code_length, mode, space_interval, space_amount)
        codes.add(code)

    return codes


def get_code_length_needed(number_of_entries):
    """Get code length needed to support one unique code for each entry.

    Parameters
    ----------
    number_of_entries : int
        Number of codewords

    Return
    ------
    code_length: int
        Lenght of code
    """
    # Minimum code length 2
    code_length = 2
    while len(string.ascii_uppercase) ** code_length < number_of_entries:
        code_length = code_length + 1

    return code_length


def insert_spaces(code, interval, space_amount=1):
    """Insert space after every x'th character, x = interval.

    Parameters
    ----------
    code : string
        String to add spaces to.
    interval : int
        Interval for inserting spaces.
    space_amount : int
        Amount of spaces per interval, by default 1.

    Returns
    -------
    string
        code separated with spaces.
    """
    # Convert to list to insert spaces between characters
    code = list(code)
    for i in range(interval - 1, len(code) - 1, interval):
        code[i] += " " * space_amount

    return "".join(code)
