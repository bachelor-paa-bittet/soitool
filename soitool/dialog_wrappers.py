"""Includes methods that wrap Qt dialogs to reduce code duplication."""

from PySide2.QtWidgets import QMessageBox


def exec_qmessagebox_dialog(window_title, text, informative_text, icon):
    """Execute QMessageBox dialog with feedback to user.

    Just a wrapper around QMessageBox.

    Parameters
    ----------
    text : str
        sets header of dialog. See
        https://doc.qt.io/qt-5/qmessagebox.html#text-prop
    informative_text : str
        sets body of dialog. See
        https://doc.qt.io/qt-5/qmessagebox.html#informativeText-prop
    icon : QMessageBox.Icon
        icon to show the user. Use one of the predefined icons as defined here:
        https://doc.qt.io/qt-5/qmessagebox.html#Icon-enum
    """
    error_message = QMessageBox()
    error_message.setWindowTitle(window_title)
    error_message.setText(text)
    error_message.setInformativeText(informative_text)
    error_message.setIcon(icon)
    error_message.exec_()


def exec_critical_dialog(text, informative_text):
    """Execute QMessageBox dialog with critical icon.

    This is simply a wrapper displaying a particular icon. See the
    documentation for exec_qmessagebox_dialog for parameter docs.
    """
    exec_qmessagebox_dialog(
        "Error", text, informative_text, QMessageBox.Critical
    )


def exec_warning_dialog(text, informative_text):
    """Execute QMessageBox dialog with warning icon.

    This is simply a wrapper displaying a particular icon. See the
    documentation for exec_qmessagebox_dialog for parameter docs.
    """
    exec_qmessagebox_dialog(
        "Advarsel", text, informative_text, QMessageBox.Warning
    )


def exec_info_dialog(text, informative_text):
    """Execute QMessageBox dialog with info icon.

    This is simply a wrapper displaying a particular icon. See the
    documentation for exec_qmessagebox_dialog for parameter docs.
    """
    exec_qmessagebox_dialog(
        "Info", text, informative_text, QMessageBox.Information
    )
