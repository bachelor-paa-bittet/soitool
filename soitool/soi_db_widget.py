"""Module containing a widget for viewing and opening SOI's from database."""
from PySide2.QtWidgets import QWidget, QHBoxLayout
from soitool.soi_model_view import SOITableView


class SOIDbWidget(QWidget):
    """Widget for viewing and opening SOI's from database."""

    def __init__(self, database, tab_widget):
        super().__init__()

        self.view = SOITableView(database, tab_widget)
        self.create_and_set_layout()

    def create_and_set_layout(self):
        """Create layout, add widget and set layout."""
        hbox = QHBoxLayout()
        hbox.addWidget(self.view)
        self.setLayout(hbox)
