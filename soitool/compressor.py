"""Functions for compression and decompression."""

import lzma
import json
import codecs


def compress(obj):
    """
    Compress a json compatible object using the lzma algorithm.

    JSON compatible object are python types that are possible to convert to
    JSON types. Se overview in this link:
    https://docs.python.org/3/library/json.html#py-to-json-table

    Parameters
    ----------
    obj : json compatible object
        Object to compress

    Returns
    -------
    string
        The compressed data casted from bytest to string
    """
    obj = json.dumps(obj)
    obj = obj.encode(encoding="ascii")
    return str(lzma.compress(obj))


def decompress(lzma_compressed_json_string):
    """
    Decompress a lzma-compressed json string.

    Decompresses a json compatible python object which is casted from bytes to
    string.

    Parameters
    ----------
    lzma_compressed_json_string : string
        The string of the lzma-compressed json string

    Returns
    -------
    python object
        One of these python objects:
        https://docs.python.org/3/library/json.html#py-to-json-table
    """
    # Remove extra b' notation
    lzma_compressed_json_string = lzma_compressed_json_string[2:-1].encode(
        encoding="ascii"
    )
    # Remove double \\
    lzma_compressed_json_string = codecs.escape_decode(
        lzma_compressed_json_string, "hex"
    )
    # Decompress
    lzma_compressed_json_string = lzma.decompress(
        lzma_compressed_json_string[0]
    )
    # Cast to ascii string
    lzma_compressed_json_string = lzma_compressed_json_string.decode(
        encoding="ascii"
    )
    # Return as python datastructure
    return json.loads(lzma_compressed_json_string)
