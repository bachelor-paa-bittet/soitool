"""Includes the functionality to edit settings for SOI.

This dialog is called when a button in soi_workspace_widget is pressed

"""

from PySide2.QtWidgets import (
    QVBoxLayout,
    QFormLayout,
    QLabel,
    QLineEdit,
    QRadioButton,
    QComboBox,
    QGroupBox,
)
from soitool.soi import (
    STRING_TO_ALGORITHM_RECTPACK_BIN,
    STRING_TO_ALGORITHM_RECTPACK_PACK,
)
from soitool.accept_reject_dialog import AcceptRejectDialog

# Following strings are variables because they are used multiple places in the
# file
ALGORITHM_BIN_BFF_USER_TEXT = "Plasser på første ledige side"
ALGORITHM_BIN_BBF_USER_TEXT = "Plasser på beste side"
ALGORITHM_SORT_NONE_USER_TEXT = "Egendefinert rekkefølge"
ALGORITHM_SORT_AREA_USER_TEXT = "Areal"
ALGORITHM_SORT_WIDTH_USER_TEXT = "Bredde"
ALGORITHM_SORT_HEIGHT_USER_TEXT = "Høyde"


class Setup(AcceptRejectDialog):  # pylint: disable = R0902
    """Contains the settings for the SOI.

    This class is used to change the settings for the SOI.
    There are 4 different settings-groups; headerdata, orientation, placement
    and algorithm. In headerdata you can change the text that appears in the
    header of the SOI. Orientation is whether you want the SOI in landscape or
    portrait. Placement is if you want to move the modules yourself or let the
    program do it. And algorithm is which algorithms should be used to place
    the modules on the SOI.

    Parameters
    ----------
    soi : soitool.soi.SOI
        The SOI-instance to change
    """

    def __init__(self, soi):
        super().__init__()

        self.soi = soi

        # Layouts
        self.layout_setup = QVBoxLayout()
        self.layout_header = QFormLayout()
        self.layout_orientation_button = QVBoxLayout()
        self.layout_alg_button_bin = QVBoxLayout()
        self.layout_alg_button_pack = QVBoxLayout()
        self.layout_alg_button_sort = QVBoxLayout()
        self.layout_placement_button = QVBoxLayout()

        # Groupbox for radiobuttons
        self.group_orientation = QGroupBox()
        self.group_placement = QGroupBox()
        self.group_algorithm_bin = QGroupBox()
        self.group_algorithm_pack = QGroupBox()
        self.group_algorithm_sort = QGroupBox()

        # Labels for mainheadings
        self.label_header = QLabel("Headerdata")
        self.label_paper_orientation = QLabel("Papirretning")
        self.label_algorithm = QLabel("Alternativer for automatisk plassering")
        self.label_module_placement = QLabel("Modulplassering")

        # Headerdata
        self.label_title = QLabel("Tittel:")
        self.label_description = QLabel("Beskrivelse:")
        self.label_version = QLabel("Versjon:")
        self.label_date = QLabel("Dato:")
        self.label_valid_from = QLabel("Gyldig fra:")
        self.label_valid_to = QLabel("Gyldig til:")
        self.label_classification = QLabel("Klassifisering")

        self.edit_title = QLineEdit()
        self.edit_description = QLineEdit()
        self.edit_version = QLineEdit()
        self.edit_date = QLineEdit()
        self.edit_valid_from = QLineEdit()
        self.edit_valid_to = QLineEdit()

        self.edit_classification = QComboBox()

        # Radiobuttons
        self.rbutton_landscape = QRadioButton("Landskap")
        self.rbutton_portrait = QRadioButton("Portrett (ikke implementert)")
        self.rbutton_portrait.setEnabled(False)
        self.rbutton_auto = QRadioButton("Automatisk")
        self.rbutton_manual = QRadioButton("Manuell (ikke implementert)")
        self.rbutton_manual.setEnabled(False)
        self.rbutton_bin_bff = QRadioButton(ALGORITHM_BIN_BFF_USER_TEXT)
        self.rbutton_bin_bbf = QRadioButton(ALGORITHM_BIN_BBF_USER_TEXT)
        self.rbutton_sort_none = QRadioButton(ALGORITHM_SORT_NONE_USER_TEXT)
        self.rbutton_sort_area = QRadioButton(ALGORITHM_SORT_AREA_USER_TEXT)
        self.rbutton_sort_width = QRadioButton(ALGORITHM_SORT_WIDTH_USER_TEXT)
        self.rbutton_sort_height = QRadioButton(
            ALGORITHM_SORT_HEIGHT_USER_TEXT
        )

        # Customize ok button from superclass to something more fitting
        self.button_ok.setText("Lagre")
        self.add_widgets_to_layout()
        self.layout_content.addLayout(self.layout_setup)

    def add_widgets_to_layout(self):
        """Add each widget to layouts.

        Takes the widgets created in init and adds them
        the the different layouts.
        """
        # Headerdata
        self.layout_setup.addWidget(self.label_header)
        self.layout_header.addRow(self.label_title, self.edit_title)
        self.layout_header.addRow(
            self.label_description, self.edit_description
        )
        self.layout_header.addRow(self.label_version, self.edit_version)
        self.layout_header.addRow(self.label_date, self.edit_date)
        self.layout_header.addRow(self.label_valid_from, self.edit_valid_from)
        self.layout_header.addRow(self.label_valid_to, self.edit_valid_to)
        self.edit_classification.addItem("UGRADERT")
        self.edit_classification.addItem("BEGRENSET")
        self.edit_classification.addItem("KONFIDENSIELT")
        self.edit_classification.addItem("HEMMELIG")
        self.edit_classification.addItem("STRENGT HEMMELIG")
        self.layout_header.addRow(
            self.label_classification, self.edit_classification
        )
        self.layout_setup.addLayout(self.layout_header)

        # Paperorientation
        self.layout_setup.addWidget(self.label_paper_orientation)
        self.layout_orientation_button.addWidget(self.rbutton_landscape)
        self.layout_orientation_button.addWidget(self.rbutton_portrait)
        self.group_orientation.setLayout(self.layout_orientation_button)
        self.layout_setup.addWidget(self.group_orientation)

        # Moduleplacement
        self.layout_setup.addWidget(self.label_module_placement)
        self.layout_placement_button.addWidget(self.rbutton_auto)
        self.layout_placement_button.addWidget(self.rbutton_manual)
        self.group_placement.setLayout(self.layout_placement_button)
        self.layout_setup.addWidget(self.group_placement)

        # Placement algorithm
        self.layout_setup.addWidget(self.label_algorithm)
        self.rbuttons_option_bin = [
            QRadioButton(name) for name in STRING_TO_ALGORITHM_RECTPACK_BIN
        ]
        self.rbuttons_option_pack = [
            QRadioButton(name) for name in STRING_TO_ALGORITHM_RECTPACK_PACK
        ]

        self.layout_alg_button_bin.addWidget(QLabel("Sidevalg"))
        self.layout_alg_button_pack.addWidget(QLabel("Plasseringsalgoritme"))
        for rbutton in self.rbuttons_option_pack:
            self.layout_alg_button_pack.addWidget(rbutton)
        self.layout_alg_button_sort.addWidget(QLabel("Sorter etter"))

        self.layout_alg_button_bin.addWidget(self.rbutton_bin_bff)
        self.layout_alg_button_bin.addWidget(self.rbutton_bin_bbf)
        self.layout_alg_button_sort.addWidget(self.rbutton_sort_none)
        self.layout_alg_button_sort.addWidget(self.rbutton_sort_area)
        self.layout_alg_button_sort.addWidget(self.rbutton_sort_width)
        self.layout_alg_button_sort.addWidget(self.rbutton_sort_height)
        self.group_algorithm_bin.setLayout(self.layout_alg_button_bin)
        self.group_algorithm_pack.setLayout(self.layout_alg_button_pack)
        self.group_algorithm_sort.setLayout(self.layout_alg_button_sort)

        self.layout_setup.addWidget(self.group_algorithm_bin)
        self.layout_setup.addWidget(self.group_algorithm_pack)
        self.layout_setup.addWidget(self.group_algorithm_sort)

    # Ignoring "Too many branches"
    def accept(self):  # pylint: disable = R0912
        """Save and update the SOI with the given changes.

        Overriden to implement custom behavior on accept, executes superclass'
        implementation as final step.
        """
        # Dict will contain all changes to make
        property_changes = {}
        property_changes["title"] = self.edit_title.text()
        property_changes["description"] = self.edit_description.text()
        property_changes["version"] = self.edit_version.text()
        property_changes["date"] = self.edit_date.text()
        property_changes["valid_from"] = self.edit_valid_from.text()
        property_changes["valid_to"] = self.edit_valid_to.text()
        property_changes[
            "classification"
        ] = self.edit_classification.currentText()

        # Find which radiobutton that is checked
        if self.rbutton_landscape.isChecked():
            property_changes["orientation"] = "landscape"
        else:
            property_changes["orientation"] = "portrait"

        if self.rbutton_auto.isChecked():
            property_changes["placement_strategy"] = "auto"
        else:
            property_changes["placement_strategy"] = "manual"

        for i in self.group_algorithm_bin.findChildren(QRadioButton):
            if i.isChecked() and i.text() == ALGORITHM_BIN_BBF_USER_TEXT:
                property_changes["algorithm_bin"] = "BBF"
            if i.isChecked() and i.text() == ALGORITHM_BIN_BFF_USER_TEXT:
                property_changes["algorithm_bin"] = "BFF"

        # Loop through groupbox to find checked radiobuttons
        for i in self.group_algorithm_pack.findChildren(QRadioButton):
            if i.isChecked():
                property_changes["algorithm_pack"] = i.text()
                break

        for i in self.group_algorithm_sort.findChildren(QRadioButton):
            if i.isChecked() and i.text() == ALGORITHM_SORT_NONE_USER_TEXT:
                property_changes["algorithm_sort"] = "none"
            elif i.isChecked() and i.text() == ALGORITHM_SORT_AREA_USER_TEXT:
                property_changes["algorithm_sort"] = "area"
            elif i.isChecked() and i.text() == ALGORITHM_SORT_WIDTH_USER_TEXT:
                property_changes["algorithm_sort"] = "width"
            elif i.isChecked() and i.text() == ALGORITHM_SORT_HEIGHT_USER_TEXT:
                property_changes["algorithm_sort"] = "height"

        # Pass changes as unpacked variable list
        self.soi.update_properties(**property_changes)

        super().accept()

    def get_from_soi(self):
        """Get data from the SOI and update the dialog with the data."""
        self.edit_title.setText(self.soi.title)
        self.edit_description.setText(self.soi.description)
        self.edit_version.setText(self.soi.version)
        self.edit_date.setText(self.soi.date)
        self.edit_valid_from.setText(self.soi.valid_from)
        self.edit_valid_to.setText(self.soi.valid_to)
        self.edit_classification.setCurrentText(self.soi.classification)

        # Check the radiobutton according to SOI settings
        if self.soi.orientation == "landscape":
            self.rbutton_landscape.setChecked(True)
        else:
            self.rbutton_portrait.setChecked(True)

        if self.soi.placement_strategy == "auto":
            self.rbutton_auto.setChecked(True)
        else:
            self.rbutton_manual.setChecked(True)

        self.groupbox_find_children()

    def groupbox_find_children(self):
        """Loop through groupbox to check radiobuttons according to SOI."""
        for i in self.group_algorithm_bin.findChildren(QRadioButton):
            if (
                i.text() == ALGORITHM_BIN_BFF_USER_TEXT
                and self.soi.algorithm_bin == "BFF"
            ):
                i.setChecked(True)
            if (
                i.text() == ALGORITHM_BIN_BBF_USER_TEXT
                and self.soi.algorithm_bin == "BBF"
            ):
                i.setChecked(True)

        for i in self.group_algorithm_pack.findChildren(QRadioButton):
            if i.text() == self.soi.algorithm_pack:
                i.setChecked(True)
                break

        for i in self.group_algorithm_sort.findChildren(QRadioButton):
            if (
                i.text() == ALGORITHM_SORT_NONE_USER_TEXT
                and self.soi.algorithm_sort == "none"
            ):
                i.setChecked(True)
            elif (
                i.text() == ALGORITHM_SORT_AREA_USER_TEXT
                and self.soi.algorithm_sort == "area"
            ):
                i.setChecked(True)
            elif (
                i.text() == ALGORITHM_SORT_WIDTH_USER_TEXT
                and self.soi.algorithm_sort == "width"
            ):
                i.setChecked(True)
            elif (
                i.text() == ALGORITHM_SORT_HEIGHT_USER_TEXT
                and self.soi.algorithm_sort == "height"
            ):
                i.setChecked(True)
