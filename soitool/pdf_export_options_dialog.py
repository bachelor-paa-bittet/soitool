"""Provides a dialog where the user can select options for export to PDF."""

from PySide2.QtWidgets import (
    QVBoxLayout,
    QLabel,
    QSpinBox,
    QFormLayout,
)
from soitool.accept_reject_dialog import AcceptRejectDialog


class PdfExportOptionsDialog(AcceptRejectDialog):
    """Dialog with options for PDF export.

    When the dialog is closed, the users choices can be fetched by directly
    accessing the child widgets.
    """

    def __init__(self):
        super().__init__()
        self.setWindowTitle("PDF-eksport")

        self.label_number_of_copies = QLabel("Antall eksemplarer")

        self.spinbox_number_of_copies = QSpinBox()
        self.spinbox_number_of_copies.setMinimum(1)
        self.spinbox_number_of_copies.setValue(1)

        self.label_resolution = QLabel("Oppløsning")

        self.spinbox_resolution = QSpinBox()
        self.spinbox_resolution.setMinimum(50)
        self.spinbox_resolution.setMaximum(1200)
        self.spinbox_resolution.setSingleStep(50)
        self.spinbox_resolution.setSuffix(" dpi")
        self.spinbox_resolution.setValue(500)

        self.layout_settings = QFormLayout()

        self.layout_settings.addRow(
            self.label_number_of_copies, self.spinbox_number_of_copies,
        )

        self.layout_settings.addRow(
            self.label_resolution, self.spinbox_resolution,
        )

        self.label_resolution_explanation = QLabel(
            "Oppløsning på 500 dpi vil gi lesbar PDF med akseptabel "
            "størrelse. Merk at større oppløsning kan føre til veldig store "
            "filer, som er vanskelig å håndtere, mens mindre oppløsning kan "
            "resultere i uleselige PDFer."
        )
        self.label_resolution_explanation.setWordWrap(True)

        self.layout_wrapper = QVBoxLayout()
        self.layout_wrapper.addLayout(self.layout_settings)
        self.layout_wrapper.addWidget(self.label_resolution_explanation)

        self.layout_content.addLayout(self.layout_wrapper)
