"""Module containing a widget for viewing and editing codebook."""
from PySide2.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout
from soitool.codebook_model_view import CodebookTableView
from soitool.codebook_row_adder import CodebookRowAdder


class CodebookWidget(QWidget):
    """Widget for viewing and editing codebook."""

    def __init__(self, database):
        super().__init__()

        # Create widgets
        self.view = CodebookTableView(database)
        self.row_adder = CodebookRowAdder(database, self.view)

        self.create_and_set_layouts()

    def create_and_set_layouts(self):
        """Create layouts, add widgets and set layout."""
        # Add widgets to layouts
        vbox = QVBoxLayout()
        vbox.addWidget(self.row_adder)
        vbox.addWidget(self.view)
        hbox = QHBoxLayout()
        hbox.addLayout(vbox)

        # Set layout
        self.setLayout(hbox)
