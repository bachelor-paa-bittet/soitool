"""Includes functionality for PDF preview."""
from PySide2.QtCore import QUrl
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings


# pylint: disable=r0903
class PDFPreviewWidget(QWebEngineView):
    """Widget to preview PDFs using QWebEngineView with PDF plugin.

    Inspired by the following sources:
    * https://doc.qt.io/qt-5/qtwebengine-features.html#pdf-file-viewing
    * https://doc.qt.io/qt-5/qtwebengine-webenginewidgets-simplebrowser-example.html

    The load method interited from QWebEngineView's can be used to load a new
    URL, and the inherited reload method can be used to reload the currently
    opened URL.

    Parameters
    ----------
    initial_url : QUrl
        URL to load by default. Used for QWebEngineView's .load method
        https://doc.qt.io/qtforpython/PySide2/QtWebEngineWidgets/QWebEngineView.html#detailed-description
    """

    def __init__(self, initial_url):
        super().__init__()
        self.page().settings().setAttribute(
            QWebEngineSettings.PluginsEnabled, True
        )
        # The following setting is the default, but explicitly enabling to be
        # explicit
        self.page().settings().setAttribute(
            QWebEngineSettings.PdfViewerEnabled, True
        )
        self.load(QUrl(initial_url))
