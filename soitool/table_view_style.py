"""Fonts and CSS used by SOITableView and CodebookTableView."""

from PySide2.QtGui import QFont

# Font and background-color for horizontal header in codebook-view.
CODEBOOK_HEADER_FONT = QFont()
CODEBOOK_HEADER_FONT.setFamily("Arial")
CODEBOOK_HEADER_FONT.setPointSize(14)
CODEBOOK_HEADER_FONT.setWeight(50)
CODEBOOK_HEADER_BACKGROUND_CSS = (
    "QHeaderView::section " "{background-color:rgb(240, 240, 240)}"
)
