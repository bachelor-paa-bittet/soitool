"""Provides a superclass to use for all dialogs needing accept and reject."""

from PySide2.QtWidgets import (
    QHBoxLayout,
    QVBoxLayout,
    QPushButton,
    QDialog,
)


class AcceptRejectDialog(QDialog):
    """Dialog with accept and reject buttons.

    The class is not meant to be used directly. It is meant to be the
    superclass of all dialogs that include "accept" and "reject" -like buttons.

    Button texts have meaningful defaults, but can be overriden by subclass.

    Subclasses should include custom content by adding it to the
    'layout_content' layout.
    """

    def __init__(self):
        super().__init__()

        self.button_ok = QPushButton("Ok")
        self.button_cancel = QPushButton("Avbryt")

        self.button_ok.clicked.connect(self.accept)
        self.button_cancel.clicked.connect(self.reject)

        self.layout_content = QVBoxLayout()
        self.layout_accept_reject_buttons = QHBoxLayout()
        self.layout_accept_reject_buttons.addWidget(self.button_ok)
        self.layout_accept_reject_buttons.addWidget(self.button_cancel)
        self.layout_content_and_buttons = QVBoxLayout()
        self.layout_content_and_buttons.addLayout(self.layout_content)
        self.layout_content_and_buttons.addLayout(
            self.layout_accept_reject_buttons
        )

        self.setLayout(self.layout_content_and_buttons)
