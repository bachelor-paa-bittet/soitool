"""SOI module for arbitrary text."""
from PySide2.QtWidgets import (
    QWidget,
    QVBoxLayout,
)
from PySide2.QtCore import Qt, QSize
from PySide2.QtGui import QIcon
from soitool.modules.fit_to_contents_widgets import (
    TextEditWithSizeOfContent,
    LineEditWithSizeOfContent,
)
from soitool.modules.module_base import (
    ModuleBase,
    DEFAULT_FONT,
    prepare_line_edit_for_pdf_export,
    prepare_text_edit_for_pdf_export,
)


class Meta(type(ModuleBase), type(QWidget)):
    """Used as a metaclass to enable multiple inheritance."""


class FreeTextModule(ModuleBase, QWidget, metaclass=Meta):
    """Module for arbitrary text.

    ## Note about widget size

    This widget might be too small to be shown in a window by itself. If you
    need to show only this widget, you should show it by wrapping it in a
    QWidget.

    Parameters
    ----------
    data : list
        First index should be header, second index should be body.
    """

    def __init__(self, data=None):
        self.type = "FreeTextModule"
        QWidget.__init__(self)
        ModuleBase.__init__(self)

        self.line_edit_header = LineEditWithSizeOfContent()
        self.line_edit_header.setFont(self.headline_font)
        self.line_edit_header.setAlignment(Qt.AlignCenter)
        self.text_edit_body = TextEditWithSizeOfContent()
        self.text_edit_body.setFont(DEFAULT_FONT)

        # When the contents of these widgets change we need to manually trigger
        # adjust of size, even on self. Without adjust of size on self the
        # widget will leave behind gray color when it is shrunk in the
        # QGraphicsScene
        self.line_edit_header.textChanged.connect(self.adjustSize)
        self.text_edit_body.textChanged.connect(
            lambda: (self.text_edit_body.adjustSize(), self.adjustSize())
        )

        self.layout = QVBoxLayout()
        self.layout.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.layout.setSpacing(0)
        self.layout.setMargin(0)
        self.layout.addWidget(self.line_edit_header)
        self.layout.addWidget(self.text_edit_body)
        self.setLayout(self.layout)

        if data is not None:
            header, body = data[0], data[1]
            self.line_edit_header.setText(header)
            self.text_edit_body.setText(body)

    def sizeHint(self):
        """Overridden to give size hint that encapsulates the child widgets."""
        size_hint_text_edit_body = self.text_edit_body.sizeHint()
        size_hint_line_edit_header = self.line_edit_header.sizeHint()
        return QSize(
            max(
                size_hint_text_edit_body.width(),
                size_hint_line_edit_header.width(),
            ),
            size_hint_text_edit_body.height()
            + size_hint_line_edit_header.height(),
        )

    def resizeEvent(self, event):
        """Update geometry before handling the resizeEvent.

        See sources in module docstring.

        Parameters
        ----------
        event : QResizeEvent
            event sent by Qt
        """
        self.updateGeometry()
        super(FreeTextModule, self).resizeEvent(event)

    def get_size(self):
        """Get size of widget.

        Returns
        -------
        Tuple
            (width, height)
        """
        size = self.sizeHint()
        return (size.width(), size.height())

    def get_data(self):
        """Return list containing module data.

        Returns
        -------
        list of module content
            first index contains header, second index contains body
        """
        return [
            self.line_edit_header.text(),
            self.text_edit_body.toPlainText(),
        ]

    def prepare_for_pdf_export(self):
        """Prepare for PDF-export."""
        prepare_line_edit_for_pdf_export(self.line_edit_header)
        prepare_text_edit_for_pdf_export(self.text_edit_body)

    @staticmethod
    def get_user_friendly_name():
        """Get user-friendly name of module."""
        return "Fritekst"

    @staticmethod
    def get_icon():
        """Get icon of module."""
        return QIcon("soitool/media/freetextmodule.png")
