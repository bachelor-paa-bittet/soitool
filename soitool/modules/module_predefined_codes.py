"""SOI-module 'Forhåndsavtalte koder'."""
import string
from random import sample
from PySide2.QtWidgets import (
    QWidget,
    QTableWidget,
    QTableWidgetItem,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QDialog,
    QListWidget,
    QListWidgetItem,
    QAbstractItemView,
    QFormLayout,
    QSpinBox,
)
from PySide2 import QtGui
from PySide2.QtCore import Qt
from soitool.modules.module_base import (
    ModuleBase,
    SUB_HEADLINE_FONT,
    DEFAULT_FONT,
    resize_table,
    prepare_table_for_pdf_export,
    is_event_edit_module,
)
from soitool.accept_reject_dialog import AcceptRejectDialog

ALPHABET = string.ascii_uppercase
HEADLINE = "FORHÅNDSAVTALTE KODER"
MAXIMUM_COLUMN_HEIGHT = 1350
DEFAULT_COLUMN_HEIGHT = 200


class Meta(type(ModuleBase), type(QWidget)):
    """Used as a metaclass to enable multiple inheritance."""


class PredefinedCodesModule(ModuleBase, QWidget, metaclass=Meta):
    """QWidget representing SOI-module 'Forhåndsavtalte koder'.

    This widget has a headline, a warning-word and a layout with x amount of
    PredefinedCodesTable-objects containing all expressions and categories from
    small codebook ("Type" = "Liten" in database-table "Codebook").

    If parameter "data" is not given, the widget reads all expressions and
    categories from small codebook, and a warning-word is randomly chosen from
    database-table "CategoryWords". It then launches a dialog where the user
    can set up the module - headline, warning_word, maximum height and category
    order.
    If parameter "data" is given, the widget is built based on the contents.

    Finally, it creates one PredefinedCodesTable per category and inserts all
    expressions in the category.

    The shortcut "Ctrl + R" launches the setup-dialog so that the user can
    modify and update the module. If changes have been made to small codebook,
    the module will reflect the changes, and it will also randomly sort all
    expressions in each category (so that a new code is assigned to each one).

    The PredefinedCodesTables are placed in one or more QVBoxLayouts, which are
    placed next to each other. The QVBoxLayouts are therefore referred to as
    columns. The user can decide (through the dialog PredefinedCodesSettings)
    the maximum column-height. Each time there is not enough room for all
    tables in a column within the maximum height, a new column is created.
    The class has an attribute 'minimum_column_height', which reflects the
    height of the tallest table. The user is not allowed to set a maximum
    column-height smaller than the minimum_column_height, because one or more
    tables would not fit.

    The widget does not use more room than needed, and resizes dynamically.

    Parameters
    ----------
    database : soitool.database.Database
        Database-instance used to read from database.
    data : dict, optional
        {
            "headline": string,
            "warning_word": string,
            "maximum_column_height": int,
            "categories": [strings],
            "tables": [{
                "table_headline": string,
                "expressions": [strings]
            ]}
        }
        By default None.
    """

    def __init__(self, database, data=None):
        self.type = "PredefinedCodesModule"
        QWidget.__init__(self)
        ModuleBase.__init__(self)

        self.database = database
        self.tables = []
        self.categories = []

        if data is not None:
            self.headline = QLabel(data["headline"])
            self.warning_word = QLabel(data["warning_word"])
            self.maximum_column_height = data["maximum_column_height"]
            self.categories = data["categories"]
            self.create_tables_from_data(data)
            self.create_and_set_layout()
        else:
            # Get a random warning-word and categories from database
            warning_word = self.database.get_random_category_word().upper()
            categories_unsorted = self.database.get_categories_from_codebook(
                small=True
            )
            # Set up module
            self.run_setup(warning_word, categories_unsorted)

    def run_setup(self, warning_word, categories):
        """Launch setup-dialog, read input, create tables and set layout."""
        # Calculate height of the tallest table
        self.minimum_column_height = self.calculate_height_of_tallest_table(
            categories
        )
        # Launch dialog
        dialog = PredefinedCodesSettings(HEADLINE, warning_word, categories)
        dialog.edit_column_height.setMinimum(self.minimum_column_height)
        dialog.exec_()

        # Read dialog-input
        categories_and_expressions = self.read_from_dialog(dialog)

        # Create tables and set layout
        self.create_tables(categories_and_expressions)
        self.create_and_set_layout()

    def read_from_dialog(self, dialog):
        """Read input from dialog PredefinedCodesSettings.

        Parameters
        ----------
        dialog : PredefinedCodesSettings
            Dialog to read from.

        Returns
        -------
        dict
            With categories as keys and list of expressions as values.
        """
        # Read headline and warning word,
        # create QLabel or set text on existing QLabel
        if hasattr(self, "headline"):
            self.headline.setText(dialog.edit_headline.text())
        else:
            self.headline = QLabel(dialog.edit_headline.text())
        if hasattr(self, "warning_word"):
            self.warning_word.setText(dialog.edit_warning_word.text().upper())
        else:
            self.warning_word = QLabel(dialog.edit_warning_word.text().upper())

        self.maximum_column_height = dialog.edit_column_height.value()

        # Read categories in order
        self.categories.clear()
        for i in range(dialog.list_category_order.count()):
            self.categories.append(dialog.list_category_order.item(i).text())

        # Create dict containing categories and their expressions
        categories_and_expressions = {}
        for category in self.categories:
            expressions = self.database.get_codebook_expressions_in_category(
                category, small=True
            )
            # Add expressions sorted randomly
            categories_and_expressions[category] = sample(
                expressions, len(expressions)
            )

        return categories_and_expressions

    def create_tables(self, categories_and_expressions):
        """Create PredefinedCodesTable-objects.

        Parameters
        ----------
        categories_and_expressions : dict
            With categories as keys and list of expressions as values.
        """
        # Delete previous tables
        del self.tables[:]

        # Create new tables
        for i, category in enumerate(categories_and_expressions.keys()):
            headline = " " + ALPHABET[i] + " " + category
            table = PredefinedCodesTable(
                headline, categories_and_expressions[category]
            )
            self.tables.append(table)

    def create_tables_from_data(self, data):
        """Create PredefinedCodesTable-objects from data.

        Parameters
        ----------
        Dict
            Contains data needed, see module description.
        """
        for table_data in data["tables"]:
            table_headline = table_data["table_headline"]
            expressions = table_data["expressions"]
            table = PredefinedCodesTable(table_headline, expressions)
            self.tables.append(table)

        self.minimum_column_height = self.get_height_of_tallest_table()

    def create_and_set_layout(self):
        """Create, fill and set layout."""
        self.headline.setFont(self.headline_font)
        self.headline.setAlignment(Qt.AlignCenter)
        self.warning_word.setFont(SUB_HEADLINE_FONT)
        self.warning_word.setAlignment(Qt.AlignCenter)

        # Layout for tables
        table_layout = self.create_table_layout()

        # Layout for warning_word
        warning_word_layout = QHBoxLayout()
        warning_word_label = QLabel("Varslingsord: ")
        warning_word_layout.addWidget(warning_word_label)
        warning_word_layout.addWidget(self.warning_word)
        warning_word_layout.setAlignment(warning_word_label, Qt.AlignHCenter)
        warning_word_layout.setAlignment(self.warning_word, Qt.AlignHCenter)

        # Main layout
        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.headline)
        self.main_layout.setAlignment(self.headline, Qt.AlignHCenter)
        self.main_layout.addLayout(warning_word_layout)
        self.main_layout.setAlignment(warning_word_layout, Qt.AlignHCenter)
        self.main_layout.addLayout(table_layout)

        # Set layout and adjust size
        self.setLayout(self.main_layout)
        self.adjustSize()

    def create_table_layout(self):
        """Create and return a table-layout.

        Returns
        -------
        QHBoxLayout
            Layout containing x amount of QVBoxLayouts with tables.
        """
        # Create table-layout
        table_layout = QHBoxLayout()

        # Algorithm-explanation: Create a "column" and fill it with tables
        # until adding the next table will overshoot maximum_column_height,
        # then add a new column and repeat the process until all tables are
        # added.
        i = 0
        column_height = 0
        # While all tables are not added
        while i < len(self.tables):
            # Create a "column"
            vbox_layout = QVBoxLayout()
            max_table_width = 0
            # While there are more tables and there is room for the next table,
            # add table to column and save it's width if it is the widest table
            while (
                i < len(self.tables)
                and column_height + self.tables[i].height()
                <= self.maximum_column_height
            ):
                table = self.tables[i]
                if table.width() > max_table_width:
                    max_table_width = table.width()
                vbox_layout.addWidget(table)
                # Increase column-height
                column_height += table.height()
                i += 1

            # Column does not have room to fit the next table.
            # Make all tables in column have equal width.
            for j in range(vbox_layout.count()):
                widget = vbox_layout.itemAt(j).widget()
                column_one_width = widget.columnWidth(0)
                widget.setFixedWidth(max_table_width)
                widget.setColumnWidth(1, max_table_width - column_one_width)

            # Add a QSpacerItem so that tables in column are pushed to the top
            vbox_layout.addStretch(1)

            # Add column to the table_layout and reset column-height.
            table_layout.addLayout(vbox_layout)
            column_height = 0

        return table_layout

    def new_table_layout(self):
        """Replace current table-layout with a new one."""
        # Get current layout and remove it from main-layout.
        table_layout = self.main_layout.itemAt(2)
        self.main_layout.removeItem(table_layout)

        # Loop through table-layout's columns (QVBoxLayouts) and delete them
        # along with their widgets to make them disappear.
        columns = [table_layout.itemAt(i) for i in range(table_layout.count())]
        for column in columns:
            tables = [column.itemAt(i).widget() for i in range(column.count())]
            for table in tables:
                if table is not None:
                    table.close()
                    del table
            del column
        del table_layout

        # Create new table-layout
        table_layout = self.create_table_layout()

        # Add table-layout and adjust own size
        self.main_layout.addLayout(table_layout)
        self.adjustSize()

    def get_size(self):
        """Return size of table.

        Returns
        -------
        Tuple
            width, height.
        """
        return self.width(), self.height()

    def get_data(self):
        """Return a dict containing all module-data.

        Returns
        -------
        Dict
            {
                "headline": string,
                "warning_word": string,
                "maximum_column_height": int,
                "categories": [strings],
                "tables": [{
                    "table_headline": string,
                    "expressions": [strings]
                ]}
            }
        """
        tables = []
        # Create a dict for each table and them to tables-list
        for table in self.tables:
            # Table-headline
            item_headline = table.item(0, 0)
            if item_headline is not None:
                table_headline = item_headline.text()
            else:
                table_headline = ""
            # Table-expressions
            expressions = []
            for i in range(1, table.rowCount()):
                expressions.append(table.item(i, 1).text())
            # Create dict with table-data and add it to list
            table = {
                "table_headline": table_headline,
                "expressions": expressions,
            }
            tables.append(table)

        # Create main dict and add tables-list
        data = {
            "headline": self.headline.text(),
            "warning_word": self.warning_word.text(),
            "maximum_column_height": self.maximum_column_height,
            "categories": self.categories,
            "tables": tables,
        }

        return data

    def get_height_of_tallest_table(self):
        """Get height of the tallest table.

        Returns
        -------
        int
            Height of the tallest table.
        """
        tallest_height = 0
        for table in self.tables:
            if table.height() > tallest_height:
                tallest_height = table.height()

        return tallest_height

    def calculate_height_of_tallest_table(self, categories):
        """Find what table will be tallest and return it's height.

        Parameters
        ----------
        categories : List
            Containing categories, is used to retrieve expressions in each
            category.

        Returns
        -------
        int
            Height of the tallest table.
        """
        # Get expressions in each category, while noting the index of the
        # category with the most expressions
        all_expressions = []
        max_length = 0
        max_index = None
        for i, category in enumerate(categories):
            expressions = self.database.get_codebook_expressions_in_category(
                category, small=True
            )
            all_expressions.append(expressions)
            if len(expressions) > max_length:
                max_index = i
                max_length = len(expressions)

        # Create the tallest table and get it's height
        tallest_table = PredefinedCodesTable("", all_expressions[max_index])
        tallest_height = tallest_table.height()

        return tallest_height

    def keyPressEvent(self, event):
        """Launch PredefinedCodesSettings when key-combination is pressed."""
        if is_event_edit_module(event):
            dialog = PredefinedCodesSettings(
                self.headline.text(), self.warning_word.text(), self.categories
            )

            # Modify dialog
            dialog.edit_column_height.setMinimum(self.minimum_column_height)
            dialog.edit_column_height.setValue(self.maximum_column_height)
            dialog.button_ok.setText("Oppdater")
            dialog.button_cancel.show()

            dialog_code = dialog.exec_()
            # If user accepted, read dialog, create tables and set a
            # new table-layout
            if dialog_code == QDialog.DialogCode.Accepted:
                categories_and_expressions = self.read_from_dialog(dialog)

                self.create_tables(categories_and_expressions)
                self.new_table_layout()

        super().keyPressEvent(event)

    def prepare_for_pdf_export(self):
        """Prepare for PDF-export."""
        for table in self.tables:
            prepare_table_for_pdf_export(table)

    @staticmethod
    def get_user_friendly_name():
        """Get user-friendly name of module."""
        return "Forhåndsavtalte koder"

    @staticmethod
    def get_icon():
        """Get icon of module."""
        return QtGui.QIcon("soitool/media/predefinedcodesmodule.png")


class PredefinedCodesTable(QTableWidget):
    """Modified QTableWidget displaying predefined-codes in a category.

    This table has a headline and two columns. The headline should consist of
    a letter followed by a category. Each row contains a unique letter and an
    expression. Practically speaking, the letter from the headline + the letter
    from the row is used as a code for the expression on that row.

    Parameters
    ----------
    headline : string
        Will be the headline of the table, should be a letter
        followed by a category.
    expressions : list
        Containing expressions (string).
    """

    def __init__(self, headline, expressions):
        QTableWidget.__init__(self)

        self.setFont(DEFAULT_FONT)

        # Make cell-borders black for increased readability
        self.setStyleSheet("QTableView { gridline-color: black; }")

        # Set focus-policy to prevent PredefinedCodesModule's
        # keyPressEvent-function to be called twice when a cell is selected.
        self.setFocusPolicy(Qt.NoFocus)

        # Set row- and columncount
        self.setRowCount(len(expressions))
        self.setColumnCount(2)

        # Remove headers and scrollbars
        self.horizontalHeader().hide()
        self.verticalHeader().hide()
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        # Insert codes
        self.insert_codes()

        # Insert expressions
        for i, expression in enumerate(expressions):
            item_expression = QTableWidgetItem(expression)
            item_expression.setFlags(
                item_expression.flags() ^ Qt.ItemIsEditable
            )
            self.setItem(i, 1, item_expression)

        # Resize columns to fit contents
        self.resizeColumnsToContents()

        # Insert headline and resize table
        self.insert_headline(headline)
        resize_table(self, columns=False, has_headline=True)

    def insert_headline(self, text):
        """Insert headline.

        Parameters
        ----------
        text : string
            Text of the headline.
        """
        # Create QTableWidgetItem
        item_headline = QTableWidgetItem(text)
        item_headline.setFont(SUB_HEADLINE_FONT)
        item_headline.setFlags(item_headline.flags() ^ Qt.ItemIsEditable)

        # Insert row, item and make it span all columns
        self.insertRow(0)
        self.setItem(0, 0, item_headline)
        self.setSpan(0, 0, 1, self.columnCount())

    def insert_codes(self):
        """Insert codes A-Z in first column."""
        for i in range(self.rowCount()):
            item_code = QTableWidgetItem(ALPHABET[i])
            item_code.setTextAlignment(Qt.AlignCenter)
            item_code.setFlags(item_code.flags() ^ Qt.ItemIsEditable)
            self.setItem(i, 0, item_code)


class PredefinedCodesSettings(AcceptRejectDialog):
    """Dialog for setup and adjustment of PredefinedCodesModule.

    Parameters
    ----------
    headline : string
        Input-field for headline will be prefilled with this string.
    warning_word : string
        Input-field for warning-word will be prefilled with this string.
    categories : list
        Containing the categories (strings).
    """

    def __init__(self, headline, warning_word, categories):
        super().__init__()

        # Hide help-button, disable close-button and set window title and width
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)
        self.setWindowFlag(Qt.WindowCloseButtonHint, False)
        self.setWindowTitle("Forhåndsavtalte koder")
        self.setFixedWidth(350)

        # Headline
        self.label_headline = QLabel("Overskrift")
        self.edit_headline = QLineEdit()
        self.edit_headline.setText(headline)

        # Warning-word
        self.label_warning_word = QLabel("Varslingsord")
        self.edit_warning_word = QLineEdit()
        self.edit_warning_word.setText(warning_word)

        # Maximum column-height
        self.label_column_height = QLabel("Maksimal kolonnehøyde")
        self.edit_column_height = QSpinBox()
        self.edit_column_height.lineEdit().setReadOnly(True)
        self.edit_column_height.setRange(50, MAXIMUM_COLUMN_HEIGHT)
        self.edit_column_height.setSingleStep(50)
        self.edit_column_height.setValue(DEFAULT_COLUMN_HEIGHT)

        # Category-order
        self.label_category_order = QLabel(
            "Kategori-rekkefølge\n(dra og slipp)"
        )
        self.list_category_order = QListWidget()
        # Enable drag-and-drop
        self.list_category_order.setDragEnabled(True)
        self.list_category_order.viewport().setAcceptDrops(True)
        self.list_category_order.setDragDropMode(
            QAbstractItemView.InternalMove
        )
        # Remove horizontal scrollbar
        self.list_category_order.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff
        )
        # Add uneditable categories
        for i, category in enumerate(categories):
            item = QListWidgetItem(category)
            item.setFlags(item.flags() & ~Qt.ItemIsEditable)
            self.list_category_order.insertItem(i, item)

        self.button_ok.setText("Opprett")

        # Hide cancel-button, it is only used when modifying an existing
        # PredefinedCodesModule
        self.button_cancel.hide()

        self.create_and_set_layout()

    def create_and_set_layout(self):
        """Create layouts, add widgets and set layout."""
        # Layout for input-widgets
        self.form_layout = QFormLayout()

        # Add labels and their associated input-widgets
        self.form_layout.addRow(self.label_headline, self.edit_headline)
        self.form_layout.addRow(
            self.label_warning_word, self.edit_warning_word
        )
        self.form_layout.addRow(
            self.label_column_height, self.edit_column_height
        )
        self.form_layout.addRow(
            self.label_category_order, self.list_category_order
        )

        # Main layout
        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(self.form_layout)

        self.layout_content.addLayout(self.main_layout)
