"""Contains a selection of widgets that exactly fit their contents.

The modules are useful to display information in full without scrollbars etc.,
especially within an environment where they are allowed to grow freely, such as
a scroll area.

## Note about `QWidget.updateGeometry`

From the Qt Docs: "Call this function if the sizeHint() or sizePolicy()
have changed." https://doc.qt.io/qt-5/qheaderview.html#resizeSections
Since `QWidget.sizeHint` is how we are telling Qt the size of the widgets in
this module we need to call `QWidget.updateGeometry` whenever their size hint
changes. In practice the size hint changes whenever the content of the widgets
change.

## Sources:

* https://www.qtcentre.org/threads/47749-QTableWidget-remains-at-default-size
* https://stackoverflow.com/questions/26162387/qtableview-qtablewidget-grid-stylesheet-grid-line-width
* https://stackoverflow.com/questions/35893957/how-to-set-cell-border-and-background-color-in-qtablewidgetitem
* https://www.qtcentre.org/threads/48334-How-to-hide-QTableView-Border
* Not used here, but very interesting alterantive to
  QHeaderView.ResizeToContents:
  https://centaurialpha.github.io/resize-qheaderview-to-contents-and-interactive
* https://stackoverflow.com/questions/47710329/how-to-adjust-qtextedit-to-fit-its-contents
* https://stackoverflow.com/questions/3050537/resizing-qts-qtextedit-to-match-text-height-maximumviewportsize
"""
from PySide2.QtWidgets import (
    QTableWidget,
    QHeaderView,
    QSizePolicy,
    QTextEdit,
    QLineEdit,
)
from PySide2 import QtCore
from PySide2.QtCore import Qt, QSize
from PySide2.QtGui import QFontMetricsF


class TableWithSizeOfContent(QTableWidget):
    """Custom QTableWidget that grows to fit it's content.

    ## Known visual bug

    The size policy for this widget is Minimum, so it can grow beyond what is
    needed to fit it's contents. This will not expand the actual table however,
    just extend the border of the widget, which looks strange. A fix to this
    has not been found, but `setStretchLastSection` looks promising.
    """

    def __init__(self, *arg, **kwargs):
        super(TableWithSizeOfContent, self).__init__(*arg, **kwargs)

        # Fixed size because resizing to contents causes size that depends on
        # screen size, which we don't want..
        self.verticalHeader().setSectionResizeMode(
            QHeaderView.ResizeMode.Fixed
        )
        # Make columns fit size of content
        self.horizontalHeader().setSectionResizeMode(
            QHeaderView.ResizeMode.ResizeToContents
        )

        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        # See class docstring
        self.cellChanged.connect(self.updateGeometry)

    def minimumSizeHint(self):
        """Overridden to set the minimal size as the size hint.

        Override of https://doc.qt.io/qt-5/qwidget.html#minimumSize-prop
        because we are unable to `self.setMinimalSize` to a size under 87,87.
        Not sure why, but simply returning our sizeHint is a valid workaround,
        since we would be setting self.minimalSize to this anyway.

        Returns
        -------
        QSize
            The minimal size for this widget.
        """
        return self.sizeHint()

    def sizeHint(self):
        """Overridden to give custom size hint about widget.

        Override of https://doc.qt.io/qt-5/qwidget.html#sizeHint-prop to
        indicate to the rest of Qt that this widget has:
        * width = sum of widths of columns + 2px for each border (left & right)
        * height = sum of height of rows + 2px for each border (top & bottom)

        Returns
        -------
        QSize
            The size of this widget.
        """
        width = 0
        if not self.verticalHeader().isHidden():
            width += self.verticalHeader().width()
        for i in range(self.columnCount()):
            width += self.columnWidth(i)

        # See explanation for `height += 2` below. The same applies here
        width += 2

        height = 0
        if not self.horizontalHeader().isHidden():
            height += self.horizontalHeader().height()
        for i in range(self.rowCount()):
            height += self.rowHeight(i)

        # Add 2 to calculated height:
        # * 1px for top border
        # * 1px for bottom border
        # NOTE: With `self.setStyleSheet("QTableWidget {border-style: none}")`
        # you should not have to add 2 here, proving that 2 makes sense. If it
        # turns out to be wrong in the future, try and fiddle with the border
        # of QTableWidget (we rely on it being 1px)
        height += 2

        return QSize(width, height)

    def removeRow(self, *args, **kwargs):
        """Overridden to resize & updateGeometry. See class docstring."""
        super(TableWithSizeOfContent, self).removeRow(*args, **kwargs)

        # Even though self.horizontalHeader is set to resize mode
        # ResizeToContents it will not resize by itself in this situation.
        # The user is (seemingly) required to edit a cell before
        # self.horizontalHeader will react. Here we are manually invoking a
        # resize, since we'd like the resize to happen right away
        self.horizontalHeader().resizeSections(
            QHeaderView.ResizeMode.ResizeToContents
        )
        self.updateGeometry()

    def insertRow(self, *args, **kwargs):
        """Overridden to updateGeometry. See class docstring."""
        super(TableWithSizeOfContent, self).insertRow(*args, **kwargs)
        self.updateGeometry()

    def setItem(self, *args, **kwargs):
        """Overridden to updateGeometry. See class docstring."""
        super(TableWithSizeOfContent, self).setItem(*args, **kwargs)
        self.updateGeometry()


class TextEditWithSizeOfContent(QTextEdit):
    """Custom QTextEdit subclass that grows to fit it's text."""

    def __init__(self, *arg, **kwargs):
        super(TextEditWithSizeOfContent, self).__init__(*arg, **kwargs)

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setLineWrapMode(QTextEdit.NoWrap)

    def sizeHint(self):
        """Give size hint using width of text."""
        size = QSize(self.document().size().toSize())
        # NOTE that the following is not respected for dimensions below 100,100
        size.setWidth(max(100, size.width()))
        size.setHeight(max(100, size.height()))
        return size

    # NOTE: calling updateGeometry here is not the best solution, refer to
    # LineEditWithSizeOfContent and TableWithSizeOfContent. Should update this
    def resizeEvent(self, event):
        """Update geometry before handling the resizeEvent.

        See sources in module docstring.

        Parameters
        ----------
        event : QResizeEvent
            Event sent by Qt.
        """
        self.updateGeometry()
        super(TextEditWithSizeOfContent, self).resizeEvent(event)


class LineEditWithSizeOfContent(QLineEdit):
    """Custom QLineEdit subclass that grows to fit it's text."""

    def __init__(self, *arg, **kwargs):
        super(LineEditWithSizeOfContent, self).__init__(*arg, **kwargs)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.textChanged.connect(self.updateGeometry)

    def sizeHint(self):
        """Give size hint using width of text."""
        size_hint_parent = super(LineEditWithSizeOfContent, self).sizeHint()
        content_width = QFontMetricsF(self.fontMetrics()).horizontalAdvance(
            self.text()
        )
        # Hardcoded addition because font metrics is slightly off...
        # Likely because of the font weight
        content_width += 10
        return QSize(content_width, size_hint_parent.height())

    def minimumSizeHint(self):
        """Overriden to set the minimal size as the size hint.

        Override of https://doc.qt.io/qt-5/qwidget.html#minimumSize-prop
        because we are unable to `self.setMinimalSize` to a size under 87,87.
        Not sure why, but simply returning our sizeHint is a valid workaround,
        since we would be setting self.minimalSize to this anyway.

        Returns
        -------
        QSize
            The minimal size for this widget.
        """
        return self.sizeHint()
