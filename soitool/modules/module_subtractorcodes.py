"""Module containing SOI-module 'Subtraktorkoder'."""
import string
from PySide2.QtWidgets import QTableWidgetItem
from PySide2 import QtGui
from PySide2.QtCore import Qt
from soitool.modules.module_base import (
    resize_table,
    DEFAULT_FONT,
)
from soitool.modules.code_table_base import CodeTableBase

# Characters for first and second column
ROW_IDENTIFIERS = string.ascii_uppercase

# Maximum number of codes is the number of row identifiers
MAXIMUM_NO_OF_CODES = len(ROW_IDENTIFIERS)
START_NO_OF_CODES = 7
CODE_LENGTH = 8

# Adds space between sets of characters, 0 => no spaces
# If code is 12345678 and interval is 4, code will be 1234 5678
SPACE_INTERVAL = 4
SPACE_AMOUNT = 3

HEADLINE_TEXT = "SUBTRAKTORKODER"


class SubtractorcodesModule(CodeTableBase):
    """Modified QTablewidget representing SOI-module 'Subtraktorkoder'.

    The default widget-initialization has a headline, a row-count of
    START_NO_OF_CODES and two columns.
    The first column contains ROW_IDENTIFIERS[row-index].
    The second column contains subtractorcodes of length CODE_LENGTH, spaced
    out for readability if SPACE_INTERVAL and SPACE_AMOUNT larger than 0.

    If parameter 'data' is given, the widget initializes based on the content.
    'data' is a dict with keys "cells", "code_length", "space_interval",
    "space_amount" and "code_character_type". "cells" is a 2D list where
    cells[0] is the headline and cells[x][y] represents the value in row x,
    column y. The other keys contain an integer.

    The widget does not use more room than needed, and resizes dynamically.
    It has shortcuts for adding and removing rows.
    """

    def __init__(self, data=None):
        self.type = "SubtractorcodesModule"
        self.code_character_type = "digits"
        self.code_font = DEFAULT_FONT
        self.maximum_no_of_codes = MAXIMUM_NO_OF_CODES

        # Set default values for table to be generated
        if data is None:
            self.start_headline = HEADLINE_TEXT
            self.start_no_of_codes = START_NO_OF_CODES
            self.code_length = CODE_LENGTH
            self.space_interval = SPACE_INTERVAL
            self.space_amount = SPACE_AMOUNT

        CodeTableBase.__init__(self, data)

        # The second column is hidden because it is not used.
        self.hideColumn(1)
        resize_table(self, columns=False, has_headline=True)

        # Make cell-borders black for increased readability
        self.setStyleSheet("QTableView { gridline-color: black; }")

    def insert_row_identifiers(self, has_headline=False):
        """Insert row identifiers in first column.

        Parameters
        ----------
        has_headline : bool, optional
            True if a headline-row exists, by default False.
        """
        start_row = 1 if has_headline else 0

        # Insert row identifiers
        for i in range(start_row, self.rowCount()):
            item = QTableWidgetItem(ROW_IDENTIFIERS[i - start_row])
            item.setTextAlignment(Qt.AlignCenter)
            item.setFlags(item.flags() ^ Qt.ItemIsEditable)
            self.setItem(i, 0, item)

    def add_row(self, selected_row_index):
        """Insert row below the selected row and add data.

        Parameters
        ----------
        selected_row_index : int
            Index of the selected row.
        """
        # If maximum amount of rows not reached (- 1 to skip headline)
        if self.rowCount() - 1 < self.maximum_no_of_codes:

            # Generate unique code and insert row
            code = self.generate_unique_code()
            self.insertRow(selected_row_index + 1)

            # Insert row identifiers
            self.insert_row_identifiers(has_headline=True)

            # Insert code
            item_code = QTableWidgetItem(code)
            item_code.setTextAlignment(Qt.AlignCenter)
            item_code.setFont(self.code_font)
            item_code.setFlags(item_code.flags() ^ Qt.ItemIsEditable)
            self.setItem(selected_row_index + 1, 2, item_code)

            self.resizeRowToContents(selected_row_index + 1)
            resize_table(self, columns=False, has_headline=True)

    def remove_row(self, row_index):
        """Remove the selected row.

        Parameters
        ----------
        row_index : int
            Index of the row to remove.
        """
        self.removeRow(row_index)

        self.insert_row_identifiers(has_headline=True)
        resize_table(self, columns=False, has_headline=True)

    @staticmethod
    def get_user_friendly_name():
        """Get user-friendly name of module."""
        return "Subtraktorkoder"

    @staticmethod
    def get_icon():
        """Get icon of module."""
        return QtGui.QIcon("soitool/media/subtractorcodesmodule.png")
