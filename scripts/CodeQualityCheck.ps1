if ($args.Count -eq 0){

    $files=Get-ChildItem -recurse | 
           Where-Object {$_.name -match "[A-Za-z0-9_]+\.py" -And $_.Name -notmatch ".pyc"} | 
           % { $_.FullName }
    Write-Output "`nChecking all .py-files"
}
else{
    $files = @()

    for($i=0; $i -lt $args.Length; $i++){

        if($args[$i] -match "[A-Za-z0-9_]+\.py"){
            $files += $args[$i]
        }
        else{
            $files += Get-ChildItem -recurse $args[$i] | 
            Where-Object {$_.Name -match "[A-Za-z0-9_]+\.py" -And $_.Name -notmatch ".pyc"} |
            % { $_.FullName }
        }
    }
}

for ($i=0; $i -lt $files.Length; $i++){
    Write-Output "`n============================$(Split-Path $files[$i] -leaf)============================"
    Write-Output "===PYLINT===`n"
    pylint --rcfile=./scripts/.pylintrc $files[$i]
    Write-Output "`n===FLAKE8===`n"
    flake8 --config ./scripts/.flake8 $files[$i]
    Write-Output "`n===BANDIT===`n"
    bandit $files[$i]
    Write-Output "`n===PYDOCSTYLE===`n"
    pydocstyle.exe --convention=numpy $files[$i]
    Write-Output "`n===BLACK===`n"
    black -l 79 --check --diff $files[$i]
}
