if [[ $# -eq 0 ]]; then
    files=$(find -type f -name '*.py' )
    echo "\nChecking all .py-files"
else
    args=$@
    
    for file in "${args[@]}"; do

        if [[ $file =~ ^[A-Za-z0-9_/]+.py ]] && ! [[ $file =~ .pyc ]]; then
            files+=( "$file" )
        else
            files+=$(find $file -type f -name '*.py')
        fi
    done
fi

for file in $files; do
    printf "========================$(basename -- $file)========================\n"
    printf "\n===PYLINT===\n"
    pylint --rcfile=./scripts/.pylintrc $file
    printf "===FLAKE8===\n"
    flake8 --config ./scripts/.flake8 $file
    printf "\n===BANDIT===\n"
    bandit $file
    printf "\n===PYDOCSTYLE===\n"
    pydocstyle --convention=numpy $file
    printf "\n===BLACK===\n"
    black -l 79 --check --diff $file
done
