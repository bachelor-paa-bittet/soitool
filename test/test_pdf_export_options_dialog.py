"""Test dialog in pdf_export_options_dialog.py."""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QTimer, Qt
from PySide2.QtTest import QTest
from soitool.pdf_export_options_dialog import PdfExportOptionsDialog

# The error being ignored here is pylint telling us that 'test' is a standard
# module, so the import should be placed further up. In our case we have an
# actual custom module called 'test', so pylint is confused.
# pylint: disable=C0411
from test.test_accept_reject_dialog import dialog_code_test_helper


if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


class TestPdfExportOptionsDialog(unittest.TestCase):
    """Testcase for PdfExportOptionsDialog class."""

    def test_starts_up(self):
        """Test that the dialog can start.

        Also tests whether dialog can be closed with the enter key.
        """
        dialog = PdfExportOptionsDialog()

        def assert_window_title_of_active_dialog():
            active_widget = app.activeModalWidget()
            self.assertEqual("PDF-eksport", active_widget.windowTitle())
            QTest.keyClick(active_widget, Qt.Key_Enter)

        QTimer.singleShot(0, assert_window_title_of_active_dialog)
        dialog.exec_()

    def test_dialog_codes(self):
        """Test dialog codes using helper function."""
        dialog_code_test_helper(self, PdfExportOptionsDialog)

    def test_full_usage_scenario(self):
        """Perform selection of desired settings an accept.

        This function is heavily inspired by soitool.test_new_module_dialog
        .TestNewModuleDialog.test_full_usage_scenario
        """
        test_copies = 10
        test_resolution = 300

        def perform_selection_and_ok():
            """Perform selection.

            Select 'test_copies' copies and 'test_resolution' dpi resolution,
            then click add.
            """
            active_widget = app.activeModalWidget()

            # Double click selects text, so that we can replace it entirely
            QTest.mouseDClick(
                active_widget.spinbox_number_of_copies.lineEdit(),
                Qt.LeftButton,
            )
            QTest.keyClicks(
                active_widget.spinbox_number_of_copies, str(test_copies)
            )

            # Triple click selects text, so that we can replace it entirely
            # Triple click is needed because the spinbox includes a suffix
            QTest.mouseDClick(
                active_widget.spinbox_resolution.lineEdit(), Qt.LeftButton
            )
            QTest.mouseClick(
                active_widget.spinbox_resolution.lineEdit(), Qt.LeftButton
            )
            QTest.keyClicks(
                active_widget.spinbox_resolution, str(test_resolution)
            )

            QTest.mouseClick(active_widget.button_ok, Qt.LeftButton)

        dialog = PdfExportOptionsDialog()
        QTimer.singleShot(0, perform_selection_and_ok)
        dialog.exec()

        # Assert dialog was successfully filled

        self.assertEqual(
            test_copies, dialog.spinbox_number_of_copies.value(),
        )
        self.assertEqual(
            test_resolution, dialog.spinbox_resolution.value(),
        )
