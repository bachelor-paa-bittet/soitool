"""Tests for soitool/compressor.py."""

import unittest
import json
from soitool.compressor import compress, decompress


class CompressorTest(unittest.TestCase):
    """Compressor tests."""

    def setUp(self):
        """Load a json to use as testdata."""
        with open("soitool/testdata/codebook.json", "r") as file:
            self.original_json = json.load(file)

    def test_compress(self):
        """Test compression returns ascii and reduces size."""
        compressed = compress(self.original_json)

        # Test if compressed is ascii
        def is_ascii(string):
            """Return True if ascii, False otherwise."""
            return all(ord(char) < 128 for char in string)

        self.assertTrue(is_ascii(compressed))

        # Test if size is reduced
        smaller = len(compressed) < len(str(self.original_json))
        self.assertTrue(smaller)

    def test_decompress(self):
        """Test decompression returns same as original_json."""
        compressed = compress(self.original_json)
        decompressed = decompress(compressed)
        self.assertEqual(decompressed, self.original_json)


if __name__ == "__main__":
    unittest.main()
