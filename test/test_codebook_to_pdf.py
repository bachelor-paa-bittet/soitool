"""Test exporting codebook to PDF."""
import unittest
import os
from pathlib import Path
from datetime import datetime
from soitool import codebook_to_pdf
from soitool.database import Database

TESTDBNAME = "testDatabase"
SOITOOL_DIR = Path(__file__).parent.parent / "soitool"
TESTDBPATH = os.path.join(SOITOOL_DIR, TESTDBNAME)

SOITOOL_ROOT_PATH = Path(__file__).parent.parent


class ExportTest(unittest.TestCase):
    """Test codebook_to_pdf.py."""

    def test_generate_filename(self):
        """Test generated filename is as expected."""
        # Get current date in format YYYY_mm_dd
        today = datetime.now().strftime("%Y_%m_%d")

        # Assert correct filename for full codebook
        expected = f"Kodebok_{today}.pdf"
        actual = codebook_to_pdf.generate_filename(small=False)
        self.assertEqual(expected, actual)

        # Assert correct filename for small codebook
        expected = f"Kodebok_liten_{today}.pdf"
        actual = codebook_to_pdf.generate_filename(small=True)
        self.assertEqual(expected, actual)

    def test_generate_codebook_pdf(self):
        """Test generated PDF-file exist."""
        database = Database(TESTDBPATH)

        # Test full codebook (default)
        codebook_to_pdf.generate_codebook_pdf(database=database)
        file_name = codebook_to_pdf.generate_filename(small=False)
        file_path_full = os.path.join(SOITOOL_ROOT_PATH, file_name)
        # Assert file exists
        self.assertTrue(os.path.exists(file_path_full))

        # Test small codebook
        codebook_to_pdf.generate_codebook_pdf(database=database, small=True)
        file_name = codebook_to_pdf.generate_filename(small=True)
        file_path_small = os.path.join(SOITOOL_ROOT_PATH, file_name)
        # Assert file exists
        self.assertTrue(os.path.exists(file_path_small))

        # Delete generated files
        if os.path.exists(file_path_full):
            os.remove(file_path_full)
        if os.path.exists(file_path_small):
            os.remove(file_path_small)
        if os.path.exists(TESTDBPATH):
            database.conn.close()
            os.remove(TESTDBPATH)
