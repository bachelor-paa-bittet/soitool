"""Test dialog in setup_settings.py."""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication
from soitool.setup_settings import Setup
from soitool.soi import SOI

# The error being ignored here is pylint telling us that 'test' is a standard
# module, so the import should be placed further up. In our case we have an
# actual custom module called 'test', so pylint is confused.
# pylint: disable=C0411
from test.test_accept_reject_dialog import dialog_code_test_helper


if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


class TestSetup(unittest.TestCase):
    """TestCase for Setup dialog class."""

    def test_dialog_code(self):
        """Test that the dialog returns dialog codes as expected."""
        # Makes use of helper function from other test case
        # The lambda is used to inject a required init parameter
        dialog_code_test_helper(self, lambda: Setup(SOI()))
