"""Database tests."""
import os
from pathlib import Path
import unittest
import json
from time import sleep
from datetime import datetime
from soitool.database import Database
from soitool.soi import SOI
from soitool.coder import get_code_length_needed
from soitool.serialize_export_import_soi import serialize_soi
from soitool.compressor import compress

TESTDBNAME = "testDatabase"
SOITOOL_DIR = Path(__file__).parent.parent / "soitool"
TESTDBPATH = os.path.join(SOITOOL_DIR, TESTDBNAME)

TESTDATA_PATH = Path(__file__).parent.parent / "soitool/testdata"

# Tolerance for timed tests. Tolerating DELTA seconds of difference
DELTA = 10


class DatabaseTest(unittest.TestCase):
    """Database tests."""

    def setUp(self):
        """Connect to/create database."""
        self.database = Database(db_path=TESTDBPATH)
        self.addCleanup(self.delete_db)

    def test_connection(self):
        """Assert connection is not None."""
        self.assertIsNotNone(self.database)

    def test_category_words(self):
        """Assert contents of table CategoryWords in DB matches testdata."""
        file_path = os.path.join(TESTDATA_PATH, "CategoryWords.txt")
        f = open(file_path, "r", encoding="utf-8")

        categories_file = []
        words_file = []

        # Get number of categories on file
        no_of_categories = int(f.readline().rstrip("\\n"))

        # Loop through categories on file
        for _ in range(no_of_categories):
            # Get category and number of words in category
            line = f.readline().split(",")
            categories_file.append(line[0])
            no_of_words = int(line[1][:-1])

            # Loop through words in category
            for _ in range(no_of_words):
                words_file.append(f.readline().rstrip())
        f.close()

        # Assert equal categories in table and file
        categories_db = self.database.get_categories()
        self.assertEqual(categories_db, categories_file)

        # Retrieve data from DB
        stmt = "SELECT * FROM CategoryWords"
        queried = self.database.conn.execute(stmt).fetchall()

        # Assert equal categories in table and file
        words_db = [row[0] for row in queried]
        self.assertEqual(words_db, words_file)

    def test_codebook(self):
        """Assert function get_codebook works as expected."""
        # Get test-data from json
        file_path = os.path.join(TESTDATA_PATH, "codebook.json")
        f = open(file_path, "r", encoding="utf-8")
        expected = json.load(f)
        f.close()

        # Get data from db
        stmt = "SELECT * FROM Codebook ORDER BY Word"
        actual = self.database.conn.execute(stmt).fetchall()

        # Check same lenght
        self.assertEqual(len(expected), len(actual))

        # Get expected length of codes
        code_len = get_code_length_needed(len(expected))

        # Check equality
        for i, entry in enumerate(expected):
            self.assertEqual(entry["word"], actual[i][0])
            self.assertEqual(entry["category"], actual[i][1])
            self.assertEqual(entry["type"], actual[i][2])
            self.assertRegex(actual[i][3], "[A-Z]{" + str(code_len) + "}")

    def test_last_updated(self):
        """Assert table LastUpdated is filled when db is created."""
        stmt = "SELECT Timestamp FROM LastUpdated"

        # [:19] to skip microseconds
        last_update = self.database.conn.execute(stmt).fetchall()[0][
            "Timestamp"
        ][:19]

        # Using raw string to not make backslash escape character
        self.assertRegex(
            last_update, r"(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})"
        )

    def test_update_last_updated(self):
        """Assert that the time gets updated when running method."""
        # Get two datetimes that should only differ by sleep_time
        stmt = "SELECT Timestamp FROM LastUpdated"
        self.database.update_last_updated()
        first_time = self.database.conn.execute(stmt).fetchall()[0][
            "Timestamp"
        ]
        sleep_time = 2
        sleep(sleep_time)
        self.database.update_last_updated()
        second_time = self.database.conn.execute(stmt).fetchall()[0][
            "Timestamp"
        ]

        # Converts from string to datetime object
        first_time = datetime.strptime(first_time, "%Y-%m-%d %H:%M:%S.%f")
        second_time = datetime.strptime(second_time, "%Y-%m-%d %H:%M:%S.%f")

        # Calculates difference in seconds
        time_diff = (second_time - first_time).total_seconds()

        # Compares difference with sleep_time
        self.assertAlmostEqual(time_diff, sleep_time, delta=DELTA)

    def test_get_categories(self):
        """Assert function get_categories works as expected."""
        file_path = os.path.join(TESTDATA_PATH, "CategoryWords.txt")
        f = open(file_path, "r", encoding="utf-8")

        # Get number of categories on file
        no_of_categories = int(f.readline().rstrip())
        categories_file = []
        for _ in range(no_of_categories):
            line = f.readline().split(", ")
            categories_file.append(line[0])

            # Skip all words:
            for _ in range(int(line[1].rstrip())):
                f.readline()

        f.close()

        # Assert categories are equal
        categories_db = self.database.get_categories()
        self.assertEqual(categories_file, categories_db)

    def test_get_random_category_word(self):
        """Assert function get_random_category_word works as expected."""
        # Read all category-words
        stmt = "SELECT Word from CategoryWords"
        queried = self.database.conn.execute(stmt).fetchall()
        category_words = [row["Word"] for row in queried]

        # Generate 5 random category-words and assert that they are
        # from table CategoryWords
        generated_category_words = []
        for _ in range(5):
            category_word = self.database.get_random_category_word()
            self.assertTrue(category_word in category_words)
            generated_category_words.append(category_word)

        # Assert that all words are not equal
        self.assertTrue(len(set(generated_category_words)) > 1)

    def test_get_categories_from_codebook(self):
        """Assert function get_categories_from_codebook works as expected."""
        # Get categories in full and small codebook
        stmt = "SELECT Category FROM Codebook GROUP BY Category"
        queried = self.database.conn.execute(stmt).fetchall()
        expected_categories_full = [row["Category"] for row in queried]

        stmt = (
            "SELECT Category FROM Codebook "
            "WHERE Type='Liten' GROUP BY Category"
        )
        queried = self.database.conn.execute(stmt).fetchall()
        expected_categories_small = [row["Category"] for row in queried]

        # Get categories in full and small codebook through function
        actual_categories_full = self.database.get_categories_from_codebook()
        actual_categories_small = self.database.get_categories_from_codebook(
            small=True
        )

        # Assert actual categories matches expected categories
        self.assertEqual(actual_categories_full, expected_categories_full)
        self.assertEqual(actual_categories_small, expected_categories_small)

    def test_get_codebook(self):
        """Assert function get_codebook returns full codebook."""
        # Load full codebook
        file_path = os.path.join(TESTDATA_PATH, "codebook.json")
        f = open(file_path, "r", encoding="utf-8")
        expected = json.load(f)
        f.close()

        # Get full codebook from db and compare
        actual = self.database.get_codebook()
        self.assertEqual(len(expected), len(actual))
        code_len = get_code_length_needed(len(expected))

        for i, entry in enumerate(expected):
            self.assertEqual(entry["word"], actual[i]["word"])
            self.assertEqual(entry["category"], actual[i]["category"])
            self.assertRegex(actual[i]["code"], "[A-Z]{" + str(code_len) + "}")
            self.assertEqual(entry["type"], actual[i]["type"])

    def test_get_codebook_small(self):
        """Assert function get_codebook only return the small codebook."""
        # Load full codebook
        file_path = os.path.join(TESTDATA_PATH, "codebook.json")
        f = open(file_path, "r", encoding="utf-8")
        data = json.load(f)
        f.close()

        # Fill expected with only small codebook entries
        expected = []
        for entry in data:
            if entry["type"] == "Liten":
                expected.append(entry)

        # Get small codebook from db
        actual = self.database.get_codebook(small=True)
        # Compare lenght
        self.assertEqual(len(expected), len(actual))
        # Get expected length of codes
        code_len = get_code_length_needed(len(expected))

        # Compare contents
        for i, entry in enumerate(expected):
            self.assertEqual(entry["word"], actual[i]["word"])
            self.assertEqual(entry["category"], actual[i]["category"])
            self.assertRegex(actual[i]["code"], "[A-Z]{" + str(code_len) + "}")
            self.assertEqual(entry["type"], actual[i]["type"])

    def test_get_codebook_expressions_in_category(self):
        """Test function get_codebook_expressions_in_category."""
        # Get a category in full and small codebook
        category_full = self.database.get_categories_from_codebook()[0]
        category_small = self.database.get_categories_from_codebook(
            small=True
        )[0]

        # Get expressions from category
        stmt = "SELECT Word FROM Codebook WHERE Category=?"
        queried = self.database.conn.execute(stmt, (category_full,)).fetchall()
        expected_expressions_full = [row["Word"] for row in queried]

        stmt += " AND Type='Liten'"
        queried = self.database.conn.execute(
            stmt, (category_small,)
        ).fetchall()
        expected_expressions_small = [row["Word"] for row in queried]

        # Get expressions from function
        actual_expr_full = self.database.get_codebook_expressions_in_category(
            category_full
        )
        actual_expr_small = self.database.get_codebook_expressions_in_category(
            category_small, small=True
        )

        # Assert actual expressions matches expected expressions
        self.assertEqual(actual_expr_full, expected_expressions_full)
        self.assertEqual(actual_expr_small, expected_expressions_small)

    def test_update_codebook(self):
        """Test that the codes get updated."""
        # Get entries before and after update
        stmt = "SELECT Word, Code FROM Codebook ORDER BY Word"
        old_entries = self.database.conn.execute(stmt).fetchall()
        self.database.update_codebook()
        new_entries = self.database.conn.execute(stmt).fetchall()

        # Collect approximately score of not updated pairs since there is a
        # chance for a word to get the same code again
        pairs = 0
        number_of_entries = len(old_entries)
        for i in range(0, number_of_entries, 2):
            if old_entries[i]["Code"] == new_entries[i]["Code"]:
                pairs = pairs + 1
        # Test that at least some of the test are new
        self.assertTrue(pairs < number_of_entries)

    def test_code_length_extended_on_update(self):
        """Test code length gets extended when number of entries makes it."""
        three_letter_len = 26 ** 2
        stmt_count = "SELECT COUNT(*) FROM Codebook"
        number_of_entries = self.database.conn.execute(stmt_count).fetchall()[
            0
        ][0]

        # Codes only gets extended when number of entries pass 26**x
        if number_of_entries == (three_letter_len):
            # Get length of current codes
            stmt_code = "SELECT Code FROM Codebook"
            code_len = len(
                self.database.conn.execute(stmt_code).fetchall()[0]["Code"]
            )
            self.assertEqual(2, code_len)

            # Insert a entry to pass 26**2 entries
            stmt = "INSERT INTO Codebook (Word, Category, Code) VALUES (?,?,?)"
            self.database.conn.execute(stmt, ("676", "676", None))
            self.database.update_codebook()

            # Check that entry got inserted
            number_of_entries = self.database.conn.execute(
                stmt_count
            ).fetchall()[0][0]
            self.assertEqual(number_of_entries, three_letter_len + 1)

            # Test that codes got extended length
            code_len = len(
                self.database.conn.execute(stmt_code).fetchall()[0]["Code"]
            )
            self.assertEqual(
                code_len, get_code_length_needed(number_of_entries)
            )

        else:
            print("ERROR: Database is not 676 entries long, cant run test")
            self.assertTrue(False)  # pylint: disable=W1503

    def test_seconds_to_next_update(self):
        """Compares (24h - time slept) and the function return value."""
        seconds_in_24h = 24 * 60 * 60
        # Insert current time LastUpdated table in db
        self.database.update_last_updated()
        # Sleeps to make time difference
        sleep_time = 2
        sleep(sleep_time)
        # Calculates expected return value and gets return value
        expected_time = seconds_in_24h - sleep_time
        actual_time = self.database.seconds_to_next_update(seconds_in_24h)
        # Compares expected and function return value with
        self.assertAlmostEqual(expected_time, actual_time, delta=DELTA)

    def teset_seconds_to_next_update_complete_period(self):
        """Check that seconds to next update can returns 0 and not negative."""
        self.database.update_last_updated()
        sleep(2)
        self.assertEqual(0, self.database.seconds_to_next_update(1))

    def test_add_code(self):
        """Test add a single code to Codebook."""
        testdata = ("Testword", "Testcategory")
        stmt = "INSERT INTO Codebook (Word, Category) VALUES (?,?)"
        self.database.conn.execute(stmt, (testdata[0], testdata[1]))
        self.database.add_code_to(testdata[0])
        stmt = "SELECT Code FROM Codebook WHERE Word = ?"
        code = self.database.conn.execute(stmt, (testdata[0],)).fetchall()[0][
            "Code"
        ]
        self.assertRegex(code, "[A-Z0-9]")

    def test_insert_soi(self):
        """Test inserting an SOI to SOI-table."""
        test_title = "TestTitle"
        test_date = datetime.now().strftime("%Y-%m-%d")
        # Create and insert SOI
        soi = SOI(title=test_title, date=test_date)
        self.database.insert_or_update_soi(soi)

        # Assert only one SOI is in table
        stmt = "SELECT * FROM SOI"
        queried = self.database.conn.execute(stmt).fetchall()
        self.assertEqual(len(queried), 1)

        # Assert SOI was written correctly
        self.assertEqual(queried[0]["Title"], test_title)
        self.assertEqual(queried[0]["Version"], soi.version)
        self.assertEqual(queried[0]["Date"], test_date)
        self.assertEqual(queried[0]["SOI"], compress(serialize_soi(soi)))

        # Insert the same SOI (same title & version), assert it is overwritten.
        self.database.insert_or_update_soi(soi)
        # Assert only one SOI is in table
        queried = self.database.conn.execute(stmt).fetchall()
        self.assertEqual(len(queried), 1)

        # Insert another version of the same SOI, assert a new row is added.
        soi.version = "2"
        self.database.insert_or_update_soi(soi)
        # Assert two SOI's are in table
        queried = self.database.conn.execute(stmt).fetchall()
        self.assertEqual(len(queried), 2)

    def delete_db(self):
        """Delete generated database-file."""
        if os.path.exists(TESTDBPATH):
            self.database.conn.close()
            os.remove(TESTDBPATH)


if __name__ == "__main__":
    unittest.main()
