"""Test module_phonebook.py."""

import unittest
from PySide2.QtGui import qApp, QKeySequence
from PySide2.QtCore import Qt, QTimer
from PySide2.QtTest import QTest
from PySide2.QtWidgets import (
    QApplication,
    QCheckBox,
    QPushButton,
    QTableWidgetItem,
)
from soitool.soi import SOI
from soitool.modules.module_phonebook import PhonebookModule


if isinstance(qApp, type(None)):
    app = QApplication([])
else:
    app = qApp


class TestPhonebookModule(unittest.TestCase):
    """TestCase for default PhonebookModule."""

    def setUp(self):
        """Prepare a plane PhonebookModule."""
        self.phonebook = PhonebookModule()
        self.phonebook.show()

    def test_header(self):
        """Test header is correct."""
        self.assertEqual(self.phonebook.header.text(), "TELEFONLISTE")

    def test_resize_width(self):
        """Test able to type in table and resize to content."""
        rambo_quote = "Live for nothing or die for something."
        item = QTableWidgetItem(rambo_quote)
        old_width = self.phonebook.minimumWidth()
        self.phonebook.table.setItem(1, 0, item)
        new_width = self.phonebook.minimumWidth()
        self.assertGreater(new_width, old_width)

    def test_popup(self):
        """Test popup is shown and i able to edit columns."""
        old_column_count = visible_columns_in(self.phonebook.table)

        def popup_action():
            popup = app.activeModalWidget().layout()

            for item_index in range(popup.count()):
                current_item = popup.itemAt(item_index).widget()
                if (
                    isinstance(current_item, QCheckBox)
                    and current_item.text() == "E-post"
                ):
                    current_item.setChecked(True)
                elif isinstance(current_item, QPushButton):
                    QTest.mouseClick(current_item, Qt.LeftButton)

        QTimer.singleShot(0, popup_action)

        QTest.keySequence(
            self.phonebook, QKeySequence(Qt.ControlModifier + Qt.Key_R)
        )
        new_column_count = visible_columns_in(self.phonebook.table)
        self.assertEqual(new_column_count, old_column_count + 1)

    def test_add_row_and_resize(self):
        """Test adding row and resizes acordingly."""
        old_row_count = self.phonebook.table.rowCount()
        old_height = self.phonebook.minimumHeight()
        QTest.keySequence(
            self.phonebook, QKeySequence(Qt.ControlModifier + Qt.Key_Plus)
        )
        new_row_count = self.phonebook.table.rowCount()
        new_height = self.phonebook.minimumHeight()
        self.assertEqual(new_row_count, old_row_count + 1)
        self.assertGreater(new_height, old_height)

    def test_remove_row(self):
        """Test removing row."""
        self.phonebook.table.setRowCount(self.phonebook.table.rowCount() + 5)
        old_row_count = self.phonebook.table.rowCount()
        self.phonebook.table.setCurrentCell(1, 0)
        QTest.keySequence(
            self.phonebook, QKeySequence(Qt.ControlModifier + Qt.Key_Minus),
        )
        new_row_count = self.phonebook.table.rowCount()
        self.assertEqual(new_row_count, old_row_count - 1)

    def test_add_to_soi_smoke_test(self):
        """Test that can add to SOI successfully."""
        soi = SOI()
        test_name = "Test name"
        soi.add_module(test_name, self.phonebook, False)
        self.assertTrue(soi.module_name_taken(test_name))


class TestPhonebookModuleWithDataParameter(unittest.TestCase):
    """TestCase for PhoneBookModule with content preset."""

    def setUp(self):
        """Prepare a phonebook with preloaded content."""
        self.data = {
            "Funksjon": ["Colonel Sam", "Rambo", ""],
            "E-post": ["sam@usarmy.com", "muscle@guns.com", ""],
        }
        self.phonebook = PhonebookModule(data=self.data)
        self.phonebook.show()

    def test_column_count(self):
        """Test number of visible columns is correct."""
        self.assertEqual(
            len(self.data), visible_columns_in(self.phonebook.table)
        )

    def test_content(self):
        """Test content is loaded correctly."""
        for column_index in range(self.phonebook.table.columnCount()):
            self.phonebook.table.setCurrentCell(0, column_index)
            column_header = self.phonebook.table.currentItem().text()
            if not self.phonebook.table.isColumnHidden(column_index):
                for row_index in range(1, self.phonebook.table.rowCount()):
                    self.phonebook.table.setCurrentCell(
                        row_index, column_index
                    )
                    current_text = self.phonebook.table.currentItem().text()
                    self.assertEqual(
                        current_text, self.data[column_header][row_index - 1]
                    )

    def test_add_to_soi_smoketest(self):
        """Test that can add to SOI successfully."""
        soi = SOI()
        test_name = "Test name"
        soi.add_module(test_name, self.phonebook, False)
        self.assertTrue(soi.module_name_taken(test_name))


def visible_columns_in(table):
    """Count number of visible columns in a table.

    Parameters
    ----------
    table : QTableWidget
        The table to count visible columns in.

    Returns
    -------
    counter : int
        Number of visible columns in table.
    """
    counter = 0
    for column_index in range(table.columnCount()):
        if not table.isColumnHidden(column_index):
            counter += 1
    return counter
