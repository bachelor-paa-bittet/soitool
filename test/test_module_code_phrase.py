"""Test CodePhraseModule."""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import Qt
from PySide2.QtTest import QTest
from soitool.modules.module_code_phrase import CodePhraseModule
from soitool.soi import SOI
from soitool.database import Database

# The error being ignored here is pylint telling us that 'test' is a standard
# module, so the import should be placed further up. In our case we have an
# actual custom module called 'test', so pylint is confused.
# pylint: disable=C0411
from test.test_database import TESTDBPATH

if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp

# When forcing category use this one, that has relatively short words
TEST_CATEGORY = "Tresort"


class TestCodePhraseModuleWithSetup(unittest.TestCase):
    """TestCase for CodePhraseModule Module."""

    def setUp(self):
        """Prepare widget with module to test as child."""
        # NOTE have to put module inside wrapping widget because module by
        # itself is too small
        # self.widget = QWidget()
        # self.layout = QVBoxLayout()
        # self.freetext_module = FreeTextModule()
        # self.layout.addWidget(self.freetext_module)
        # self.widget.setLayout(self.layout)
        self.database = Database(db_path=TESTDBPATH)
        self.widget = CodePhraseModule(
            database=self.database, category=TEST_CATEGORY
        )
        self.widget.show()

    def test_type_header(self):
        """Make sure can type into header."""
        test_header = "This is a header"

        QTest.mouseDClick(self.widget.line_edit_header, Qt.LeftButton)

        QTest.keyClicks(self.widget.line_edit_header, test_header)

        self.assertEqual(test_header, self.widget.line_edit_header.text())

    def test_add_to_soi_smoke_test(self):
        """Test that can add to SOI successfully."""
        soi = SOI()
        test_name = "Test name"
        soi.add_module(test_name, self.widget, False)
        self.assertTrue(soi.module_name_taken(test_name))


class TestCodePhraseModuleWithoutSetup(unittest.TestCase):
    """TestCase for CodePhraseModule without setUp."""

    def test_create_with_data(self):
        """Test creation from data."""
        test_header = "Test"
        test_category = TEST_CATEGORY
        test_available_words = [
            "Eik",
            "Furu",
            "Gran",
            "Ask",
            "Bøk",
        ]
        test_table = [
            ["Bjørk", "Testfrase 1"],
            ["Hassel", "Testfrase 2"],
            ["Lønn", "Testfrase 3"],
        ]
        test_data = [
            test_header,
            test_category,
            test_available_words,
            test_table,
        ]

        widget = CodePhraseModule(
            database=Database(db_path=TESTDBPATH), data=test_data
        )
        widget.show()

        # Assert expected data is in real widget
        self.assertEqual(widget.line_edit_header.text(), test_header)
        self.assertEqual(widget.category, test_category)
        self.assertEqual(widget.table.rowCount(), len(test_table))
        self.assertEqual(widget.table.columnCount(), len(test_table[0]))
        for i in range(widget.table.rowCount()):
            for j in range(widget.table.columnCount()):
                item = widget.table.item(i, j)
                self.assertEqual(test_table[i][j], item.text())

        # Assert get back our data with get_data
        fetched_data = widget.get_data()
        self.assertEqual(test_data, fetched_data)
