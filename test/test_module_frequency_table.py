"""Test module_frequency_table.py."""

import unittest
from PySide2.QtWidgets import QApplication, QTableWidgetItem
from PySide2.QtGui import qApp, QKeySequence
from PySide2.QtCore import Qt, QTimer
from PySide2.QtTest import QTest
from soitool.modules.module_frequency_table import FrequencyTableModule
from soitool.soi import SOI

if isinstance(qApp, type(None)):
    app = QApplication([])
else:
    app = qApp


class TestModuleFrequencyTable(unittest.TestCase):
    """TestCase for plane FrequencyTableModule."""

    def setUp(self):
        """Prepare a plane FrequencyTableModule."""
        self.frequency_table = FrequencyTableModule()
        self.frequency_table.show()

    def test_header(self):
        """Test header is correct."""
        self.assertEqual(self.frequency_table.header.text(), "FREKVENSTABELL")

    def test_resize_width(self):
        """Test automatic resize to content in width."""
        old_width = self.frequency_table.minimumWidth()
        rocky_quote = "Lay off that pet shop dame, women weaken legs!"
        item = QTableWidgetItem(rocky_quote)
        self.frequency_table.table.setItem(2, 0, item)
        new_width = self.frequency_table.minimumWidth()
        self.assertGreater(new_width, old_width)

    def test_resize_heigth(self):
        """Test automatic resize in height when row added."""
        old_heigth = self.frequency_table.minimumHeight()
        self.frequency_table.add_row()
        new_height = self.frequency_table.minimumHeight()
        self.assertGreater(new_height, old_heigth)

    def test_add_row(self):
        """Test add row."""
        old_row_count = self.frequency_table.table.rowCount()
        QTest.keySequence(
            self.frequency_table,
            QKeySequence(Qt.ControlModifier + Qt.Key_Plus),
        )
        new_row_count = self.frequency_table.table.rowCount()
        self.assertGreater(new_row_count, old_row_count)

    def test_remove_row(self):
        """Test remove row."""
        self.frequency_table.table.setRowCount(
            self.frequency_table.table.rowCount() + 1
        )
        old_row_count = self.frequency_table.table.rowCount()
        self.frequency_table.table.setCurrentCell(2, 0)
        QTest.keySequence(
            self.frequency_table,
            QKeySequence(Qt.ControlModifier + Qt.Key_Minus),
        )
        new_row_count = self.frequency_table.table.rowCount()
        self.assertGreater(old_row_count, new_row_count)

    def test_popup_smoke_test(self):
        """Test that popup can edit module width (columns)."""
        old_width = self.frequency_table.minimumWidth()

        def popup_action():
            popup = app.activeModalWidget().layout()
            grid = popup.itemAt(0)
            btn_toggle_all = grid.itemAtPosition(2, 1).widget()
            QTest.mouseClick(btn_toggle_all, Qt.LeftButton)
            QTest.mouseClick(popup.itemAt(2).widget(), Qt.LeftButton)

        QTimer.singleShot(0, popup_action)
        QTest.keySequence(
            self.frequency_table, QKeySequence(Qt.ControlModifier + Qt.Key_R)
        )
        new_width = self.frequency_table.minimumWidth()

        self.assertGreater(new_width, old_width)

    def test_add_to_soi_smoke_test(self):
        """Test that can add to SOI successfully."""
        soi = SOI()
        test_name = "Rocky Balboa"
        soi.add_module(test_name, self.frequency_table, False)
        self.assertTrue(soi.module_name_taken(test_name))


class TestModuleFrequencyTableWithDataParameter(unittest.TestCase):
    """TestCase for FrequencyTableModule loaded from data."""

    def setUp(self):
        """Prepare a FrequencyTableModule loaded from data."""
        self.data = {
            "": {
                "Nett": ["Rocky", "Mickey", "Adrian", "Paulie"],
                "ADR": ["1", "2", "3", "4"],
            },
            "HF": {
                "Frekvenser": ["1 mHz", "2 mHz", "3 mHz", "4 mHz"],
                "Kodeord": ["Glove", "Hat", "Glasses", "Cigar"],
            },
        }
        self.frequency_table = FrequencyTableModule(data=self.data)
        self.frequency_table.show()
        # app.exec_()

    def test_content(self):
        """Test loaded content is correctly loaded into module."""
        # Because of the if sentence to avoid hidden columns and nonetype a
        # point is given every time the test enters the deepest level which
        # means it passed all the assert statements.
        points = 0
        needed_points = 0
        for main_header in self.data:
            for sub_header in self.data[main_header]:
                needed_points += len(self.data[main_header][sub_header])

        column_index = 0
        while column_index < self.frequency_table.table.columnCount():
            current_item = self.frequency_table.table.item(0, column_index)
            if current_item and not self.frequency_table.table.isColumnHidden(
                column_index
            ):
                main_header = current_item.text()
                sub_length = len(self.data[main_header])
                for _ in range(sub_length):
                    sub_header = self.frequency_table.table.item(
                        1, column_index
                    ).text()
                    self.assertTrue(
                        sub_header in self.data[main_header].keys()
                    )
                    for row_index in range(
                        2, self.frequency_table.table.rowCount()
                    ):
                        current_text = self.frequency_table.table.item(
                            row_index, column_index
                        ).text()
                        self.assertEqual(
                            current_text,
                            self.data[main_header][sub_header][row_index - 2],
                        )
                        points += 1
                column_index += 1
            else:
                column_index += 1

        self.assertEqual(needed_points, points)

    def test_add_to_soi_smoke_test(self):
        """Test that can add to SOI successfully with data parameter."""
        soi = SOI()
        test_name = "Mickey Goldmill"
        soi.add_module(test_name, self.frequency_table, False)
        self.assertTrue(soi.module_name_taken(test_name))
