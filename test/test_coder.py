"""Coder tests."""
import unittest
from itertools import permutations
from string import ascii_uppercase
from soitool import coder


class CoderTest(unittest.TestCase):
    """Test coder.py."""

    LEN = 2
    COMBINATIONS = len(ascii_uppercase) ** LEN

    def test_get_code_ascii(self):
        """Test code is uppercase alphabetic and correct length."""
        code = coder.get_code(3)
        self.assertRegex(code, "[A-Z]{3}")

    def test_get_code_digits(self):
        """Test code is only digits and correct length."""
        code = coder.get_code(3, "digits")
        self.assertRegex(code, "[0-9]{3}")

    def test_get_code_combo(self):
        """Assert code is both digits and alphabetic characters."""
        code = coder.get_code(100, "combo")
        self.assertRegex(code, "[A-Z0-9]{100}")

    def test_get_code_set(self):
        """Test all codes are unique."""
        codes = coder.get_code_set(self.COMBINATIONS, self.LEN)
        self.assertEqual(self.COMBINATIONS, len(codes))

        # All possible permutations need to be inn codes
        permutations_li = list(permutations(ascii_uppercase, self.LEN))
        flag = False

        for perm in permutations_li:
            if str(perm[0] + perm[1]) not in codes:
                flag = True
                break

        self.assertFalse(flag)

    def test_get_code_length(self):
        """Test return correct codelenght."""
        self.assertEqual(self.LEN, coder.get_code_length_needed(0))
        self.assertEqual(
            self.LEN, coder.get_code_length_needed(self.COMBINATIONS)
        )
        self.assertEqual(
            3, coder.get_code_length_needed(self.COMBINATIONS + 1)
        )


if __name__ == "__main__":
    unittest.main()
