"""Test serializing, exporting and importing of SOI."""
import os
import unittest
import json
from pathlib import Path
from datetime import datetime
from test.test_database import TESTDBPATH
from PySide2.QtWidgets import QApplication
from soitool.soi import SOI
from soitool.modules.module_table import TableModule
from soitool.database import Database
from soitool.serialize_export_import_soi import (
    serialize_soi,
    export_soi,
    import_soi,
    SERIALIZED_SOI_SCHEMA,
)

# The error being ignored here is pylint telling us that 'test' is a standard
# module, so the import should be placed further up. In our case we have an
# actual custom module called 'test', so pylint is confused.
# pylint: disable=C0411
from test.test_soi import deep_copy_of_modules_list

app = QApplication.instance()
if app is None:
    app = QApplication([])

SOITOOL_ROOT_PATH = Path(__file__).parent.parent

# SOI content
TITLE = "testSOI"
DESCRIPTION = "This is a description"
VERSION = "1"
DATE = "2020-01-01"
VALID_FROM = "2020-01-01"
VALID_TO = "2020-01-02"
ICON = "soitool/media/HVlogo.png"
CLASSIFICATION = "UGRADERT"
ORIENTATION = "portrait"
PLACEMENT_STRATEGY = "manual"
ALGORITHM_BIN = "BFF"
ALGORITHM_PACK = "MaxRectsBl"
ALGORITHM_SORT = "area"
MODULES = [
    {
        "widget": TableModule(data=[["H1"], ["Row1"], ["Row2"]],),
        "meta": {"x": 0, "y": 0, "page": 1, "name": "Table1"},
    },
    {
        "widget": TableModule(data=[["H1", "H2"], ["Row1Col1", "Row1Col2"]],),
        "meta": {"x": 200, "y": 150, "page": 1, "name": "Table1"},
    },
]


# We'd like to use the same list of modules for both main and attachment
# modules, so we need this to help make a copy of widgets. Otherwise main and
# attachment modules would be referencing the same underlying widgets, as Qt
# does not support cloning of QWidgets by default.
def tablemodule_cloner(module):
    """Return new TableModule that is a clone of parameter module.

    Parameters
    ----------
    module : TableModule
        Module to clone.

    Returns
    -------
    TableModule
        New TableModule that is a clone of parameter module.
    """
    return TableModule(data=module.get_data())


class SerializeTest(unittest.TestCase):
    """Testcase for functions in module 'serialize_export_import_soi.py'."""

    def setUp(self):
        """Create SOI-object and generate filepath for exported SOI."""
        self.soi = SOI(
            title=TITLE,
            description=DESCRIPTION,
            version=VERSION,
            date=DATE,
            valid_from=VALID_FROM,
            valid_to=VALID_TO,
            icon=ICON,
            classification=CLASSIFICATION,
            orientation=ORIENTATION,
            placement_strategy=PLACEMENT_STRATEGY,
            algorithm_bin=ALGORITHM_BIN,
            algorithm_pack=ALGORITHM_PACK,
            algorithm_sort=ALGORITHM_SORT,
            modules=deep_copy_of_modules_list(MODULES, tablemodule_cloner),
            attachments=deep_copy_of_modules_list(MODULES, tablemodule_cloner),
        )
        date = datetime.strptime(DATE, "%Y-%m-%d")
        date_formatted_for_file_path = date.strftime("%Y_%m_%d")
        file_name_uncompressed = (
            f"SOI_{TITLE}_{date_formatted_for_file_path}.json"
        )
        file_name_compressed = (
            f"SOI_{TITLE}_{date_formatted_for_file_path}.txt"
        )

        self.file_path_uncompressed = os.path.join(
            SOITOOL_ROOT_PATH, file_name_uncompressed
        )
        self.file_path_compressed = os.path.join(
            SOITOOL_ROOT_PATH, file_name_compressed
        )
        self.database = Database(db_path=TESTDBPATH)

    def test_serialize_soi(self):
        """Serialize SOI and check its validity against schema."""
        # Serialize SOI and load as dict
        serialized = serialize_soi(self.soi)
        json_data = json.loads(serialized)
        # Assert serialized SOI format matches schema
        self.assertTrue(SERIALIZED_SOI_SCHEMA.is_valid(json_data))

    def test_export_soi(self):
        """Export SOI and assert files were created."""
        # Export SOI
        export_soi(self.soi, compressed=False)
        export_soi(self.soi, compressed=True)

        # Assert files exist
        self.assertTrue(os.path.exists(self.file_path_uncompressed))
        self.assertTrue(os.path.exists(self.file_path_compressed))

    def test_import_soi_invalid(self):
        """Assert invalid serialization throws error on import."""
        # Write invalid serialization to file
        invalid_soi_file_path = "test_file.json"
        invalid_serialization = {
            "title": "SOI-test",
            "invalid_key": "All keys do not exist",
        }
        file = open(invalid_soi_file_path, "w")
        file.write(json.dumps(invalid_serialization))
        file.close()

        # Assert import throws error
        with self.assertRaises(ValueError) as error:
            import_soi(invalid_soi_file_path, self.database)
        self.assertEqual(
            str(error.exception),
            "Serialized SOI does not have correct format.",
        )
        if os.path.exists(invalid_soi_file_path):
            os.remove(invalid_soi_file_path)

    def test_import_soi(self):
        """Import serialized SOI's, check content and delete SOI-files."""
        # Import uncompressed and compressed SOI's
        soi_uncompressed = import_soi(
            self.file_path_uncompressed, self.database
        )
        soi_compressed = import_soi(self.file_path_compressed, self.database)

        # Assert SOI content is correct
        self.check_soi_content(soi_uncompressed)
        self.check_soi_content(soi_compressed)

        # Delete exported SOI-files
        if os.path.exists(self.file_path_compressed):
            os.remove(self.file_path_compressed)
        if os.path.exists(self.file_path_uncompressed):
            os.remove(self.file_path_uncompressed)

    def check_soi_content(self, soi):
        """Assert SOI-content is correct.

        Parameters
        ----------
        soi : soitool.soi.SOI
            SOI-instance with content to check.
        """
        # Assert SOI content is correct
        self.assertEqual(TITLE, soi.title)
        self.assertEqual(DESCRIPTION, soi.description)
        self.assertEqual(VERSION, soi.version)
        self.assertEqual(DATE, soi.date)
        self.assertEqual(VALID_FROM, soi.valid_from)
        self.assertEqual(VALID_TO, soi.valid_to)
        self.assertEqual(ICON, soi.icon)
        self.assertEqual(CLASSIFICATION, soi.classification)
        self.assertEqual(ORIENTATION, soi.orientation)
        self.assertEqual(PLACEMENT_STRATEGY, soi.placement_strategy)
        self.assertEqual(ALGORITHM_BIN, soi.algorithm_bin)
        self.assertEqual(ALGORITHM_PACK, soi.algorithm_pack)
        self.assertEqual(ALGORITHM_SORT, soi.algorithm_sort)
        self.assertEqual(type(MODULES[0]["widget"]), TableModule)
        self.assertEqual(type(MODULES[1]["widget"]), TableModule)
        self.assertEqual(MODULES[0]["meta"], soi.modules[0]["meta"])
        self.assertEqual(MODULES[1]["meta"], soi.modules[1]["meta"])
