"""Test functions in dialog_wrappers.py.

Tests are performed by launching the dialogs and inspecting their icons. To
test the dialogs we need to 'load' our testing function using
QTimer.singleShot before starting the widget. This will in practice let us run
our code after the dialog started, even though it is modal.
"""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication, QMessageBox
from PySide2.QtCore import QTimer, Qt
from PySide2.QtTest import QTest
from soitool.dialog_wrappers import (
    exec_qmessagebox_dialog,
    exec_warning_dialog,
    exec_critical_dialog,
    exec_info_dialog,
)

if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


class TestDialogWrappers(unittest.TestCase):
    """TestCase for dialog wrapper functions."""

    def assert_that_dialog_has_icon_and_close(self, icon):
        """Assert icon of current dialog and close it by pressing enter."""
        active_widget = app.activeModalWidget()
        self.assertEqual(icon, active_widget.icon())
        QTest.keyClick(active_widget, Qt.Key_Enter)

    def test_qmessagebox_wrapper(self):
        """Test the general QMessageBox wrapper."""
        QTimer.singleShot(
            0,
            lambda: self.assert_that_dialog_has_icon_and_close(
                QMessageBox.NoIcon
            ),
        )
        exec_qmessagebox_dialog(
            "title", "text", "informative text", QMessageBox.NoIcon
        )

    def test_critical_dialog(self):
        """Test the critical message wrapper."""
        QTimer.singleShot(
            0,
            lambda: self.assert_that_dialog_has_icon_and_close(
                QMessageBox.Critical
            ),
        )
        exec_critical_dialog("text", "informative text")

    def test_info_dialog(self):
        """Test the info message wrapper."""
        QTimer.singleShot(
            0,
            lambda: self.assert_that_dialog_has_icon_and_close(
                QMessageBox.Information
            ),
        )
        exec_info_dialog("text", "informative text")

    def test_warning_dialog(self):
        """Test the warning message wrapper."""
        QTimer.singleShot(
            0,
            lambda: self.assert_that_dialog_has_icon_and_close(
                QMessageBox.Warning
            ),
        )
        exec_warning_dialog("text", "informative text")
