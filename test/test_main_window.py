"""Test main_window.py."""
import os
import unittest
from test.test_database import TESTDBPATH
from PySide2 import QtWidgets, QtGui
from soitool import main_window

if isinstance(QtGui.qApp, type(None)):
    app = QtWidgets.QApplication([])
else:
    app = QtGui.qApp


class TestMainWindow(unittest.TestCase):
    """Test class for main_window.py."""

    def setUp(self):
        """Set up main_window_widget for testing."""
        self.test_mw = main_window.MainWindow(db_path=TESTDBPATH)
        self.addCleanup(self.delete_db)

    def test_window_title(self):
        """Test window title is correct."""
        expected = "SOI-tool"
        actual = self.test_mw.windowTitle()
        self.assertEqual(expected, actual)

    # To smoke test SOIWorkspaceWidget
    def test_new_module_btn(self):
        """Test correct label on new module button."""
        expected = "Ny modul"
        actual = self.test_mw.tabs.currentWidget().button_new_module.text()
        self.assertEqual(expected, actual)

    def delete_db(self):
        """Delete generated 'testDatabase'-file."""
        if os.path.exists(TESTDBPATH):
            self.test_mw.database.conn.close()
            os.remove(TESTDBPATH)


if __name__ == "__main__":
    unittest.main()
