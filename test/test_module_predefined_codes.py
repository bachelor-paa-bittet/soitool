"""Tests for PredefinedCodesModule."""
import unittest
import string
from test.test_database import TESTDBPATH
from PySide2.QtCore import Qt, QTimer
from PySide2 import QtGui
from PySide2.QtTest import QTest
from PySide2.QtWidgets import QApplication
from soitool.database import Database
from soitool.modules.module_predefined_codes import (
    PredefinedCodesModule,
    HEADLINE,
    DEFAULT_COLUMN_HEIGHT,
)

ALPHABET = string.ascii_uppercase

if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


class TestDefaultPredefinedCodesModule(unittest.TestCase):
    """TestCase for PredefinedCodesModule."""

    def setUp(self):
        """Create database and PredefinedCodesModule."""
        self.database = Database(db_path=TESTDBPATH)

        # Get categories in small codebook
        self.categories = self.database.get_categories_from_codebook(
            small=True
        )

        def press_enter_on_active_dialog():
            active_widget = app.activeModalWidget()
            QTest.keyClick(active_widget, Qt.Key_Enter)

        QTimer.singleShot(0, press_enter_on_active_dialog)
        self.module = PredefinedCodesModule(self.database)

    def test_default_dialog(self):
        """Test default values in dialog."""

        def test_and_close_dialog():
            dialog = app.activeModalWidget()

            # Assert prefilled headline is correct
            self.assertEqual(dialog.edit_headline.text(), HEADLINE)

            # Assert prefilled warning-word is not empty
            warning_word = dialog.edit_warning_word.text()
            self.assertTrue(len(warning_word) > 0)

            # Assert prefilled maximum column-height is correct
            maximum_column_height_value = dialog.edit_column_height.value()
            self.assertTrue(
                maximum_column_height_value >= DEFAULT_COLUMN_HEIGHT
            )

            # Assert categories are correct
            for i, category in enumerate(self.categories):
                dialog_category = dialog.list_category_order.item(i).text()
                self.assertEqual(category, dialog_category)

            # Close dialog
            dialog.accept()

        # Use shortcut to open dialog
        QTimer.singleShot(0, test_and_close_dialog)
        QTest.keyClicks(self.module, "R", Qt.ControlModifier)

    def test_default_module(self):
        """Test default module."""
        # Assert headline is correct
        self.assertEqual(self.module.headline.text(), HEADLINE)

        # Assert warning-word is not empty
        self.assertTrue(len(self.module.warning_word.text()) > 0)

        # Assert module has one table per category
        self.assertEqual(len(self.module.tables), len(self.categories))

        # Assert each table has the correct headline and contents
        for i, table in enumerate(self.module.tables):
            expected_headline = " " + ALPHABET[i] + " " + self.categories[i]
            actual_headline = table.item(0, 0).text()
            self.assertEqual(actual_headline, expected_headline)

            expressions = self.database.get_codebook_expressions_in_category(
                self.categories[i], small=True
            )
            # Assert codes are correct and read expressions in table
            actual_expressions = []
            for j in range(1, table.rowCount()):
                expected_code = ALPHABET[j - 1]
                actual_code = table.item(j, 0).text()
                self.assertEqual(actual_code, expected_code)

                actual_expressions.append(table.item(j, 1).text())
            # Assert expressions are correct
            for actual_expression in actual_expressions:
                self.assertTrue(actual_expression in expressions)


class TestDefaultPredefinedCodesModuleFromData(unittest.TestCase):
    """TestCase for initializing PredefinedCodesModule from data."""

    def test_create_from_data(self):
        """Test creating PredefinedCodesModule from data."""
        test_data = {
            "headline": "HeadlineText",
            "warning_word": "WarningWordText",
            "maximum_column_height": 500,
            "categories": ["CategoryOne", "CategoryTwo"],
            "tables": [
                {
                    "table_headline": "TableHeadlineOne",
                    "expressions": ["expressionOne", "expressionTwo"],
                },
                {
                    "table_headline": "TableHeadlineTwo",
                    "expressions": ["expressionOne", "expressionTwo"],
                },
            ],
        }
        # Create PredefinedCodesModule from data
        module = PredefinedCodesModule(Database(db_path=TESTDBPATH), test_data)

        # Assert headline is correct
        self.assertEqual(module.headline.text(), "HeadlineText")

        # Assert warning-word is correct
        self.assertEqual(module.warning_word.text(), "WarningWordText")

        # Assert module has two tables
        self.assertEqual(len(module.tables), 2)

        # Assert each table has the correct headline and expressions
        table_one = module.tables[0]
        table_two = module.tables[1]

        # Assert correct headlines
        self.assertEqual(table_one.item(0, 0).text(), "TableHeadlineOne")
        self.assertEqual(table_two.item(0, 0).text(), "TableHeadlineTwo")

        # Assert correct codes and expressions
        self.assertEqual(table_one.item(1, 1).text(), "expressionOne")
        self.assertEqual(table_one.item(2, 1).text(), "expressionTwo")
        self.assertEqual(table_two.item(1, 1).text(), "expressionOne")
        self.assertEqual(table_two.item(2, 1).text(), "expressionTwo")
        for i in range(1, 3):
            self.assertEqual(table_one.item(i, 0).text(), ALPHABET[i - 1])
            self.assertEqual(table_two.item(i, 0).text(), ALPHABET[i - 1])
