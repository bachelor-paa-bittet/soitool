"""Test FreeTextModule."""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication, QWidget, QVBoxLayout
from PySide2.QtCore import Qt
from PySide2.QtTest import QTest
from soitool.modules.module_freetext import FreeTextModule
from soitool.soi import SOI

if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


class TestFreeTextModuleWithSetUp(unittest.TestCase):
    """TestCase for FreeText Module."""

    def setUp(self):
        """Prepare widget with module to test as child."""
        # NOTE have to put module inside wrapping widget because module by
        # itself is too small
        self.widget = QWidget()
        self.layout = QVBoxLayout()
        self.freetext_module = FreeTextModule()
        self.layout.addWidget(self.freetext_module)
        self.widget.setLayout(self.layout)
        self.widget.show()

    def test_type_header_and_body(self):
        """Type into header and body and assert expected content."""
        test_header = "This is a header"
        test_body = "This is a body"

        QTest.keyClicks(self.freetext_module.line_edit_header, test_header)
        QTest.keyClicks(self.freetext_module.text_edit_body, test_body)

        self.assertEqual(
            test_header, self.freetext_module.line_edit_header.text()
        )
        self.assertEqual(
            test_body, self.freetext_module.text_edit_body.toPlainText()
        )

    def test_module_dynamic_size_body(self):
        """Type content into body to make it grow, and assert growth."""
        before_width, before_height = self.freetext_module.get_size()

        # Make enough newlines into the body to force the module to grow
        # vertically
        for _ in range(30):
            QTest.keyClick(self.freetext_module.text_edit_body, Qt.Key_Enter)

        # Insert enough keys in the body to force the module to grow
        # horizontally
        for _ in range(100):
            QTest.keyClick(self.freetext_module.text_edit_body, Qt.Key_A)

        after_width, after_height = self.freetext_module.get_size()

        # Make sure the module grew
        self.assertTrue(before_width < after_width)
        self.assertTrue(before_height < after_height)

    def test_add_to_soi_smoke_test(self):
        """Test that can add to SOI successfully."""
        soi = SOI()
        test_name = "Test name"
        soi.add_module(test_name, self.freetext_module, False)
        self.assertTrue(soi.module_name_taken(test_name))


class TestFreeTextModuleWithoutSetUp(unittest.TestCase):
    """TestCase for FreeTextModule without setUp."""

    def test_create_with_data(self):
        """Test creation from data."""
        test_header = "This is the header"
        test_body = "This is the body"
        test_data = [test_header, test_body]
        freetext_module = FreeTextModule(data=test_data)
        freetext_module.show()

        # Assert expected data is in real widget
        self.assertEqual(freetext_module.line_edit_header.text(), test_header)
        self.assertEqual(
            freetext_module.text_edit_body.toPlainText(), test_body
        )

        # Assert get back our data with get_data
        fetched_data = freetext_module.get_data()
        self.assertEqual(test_data, fetched_data)
