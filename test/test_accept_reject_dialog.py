"""Test dialog in accept_reject_dialog.py.

Also provides a function 'dialog_code_test_helper' that can be used by tests of
subclasses of AcceptRejectDialog to ensure that dialog codes work as expected.
"""

import unittest
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication, QDialog
from PySide2.QtCore import QTimer, Qt
from PySide2.QtTest import QTest
from soitool.accept_reject_dialog import AcceptRejectDialog


if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp


def dialog_code_test_helper(testcase, dialog_class):
    """Test that a given dialog class returns dialog codes as expected.

    Parameters
    ----------
    testcase : unittest.TestCase
        Used to report status of test.
    dialog_class : AcceptRejectDialog or derivative
        The dialog class must be a derivative of AcceptRejectDialog, or
        AcceptRejectDialog itself. This is because it's properties are used for
        testing. Also accepts a callable that returns an instance of a
        derivative of AcceptRejectDialog, or AcceptRejectDialog itself. This is
        useful when the subclass takes custom init parameters.
    """
    # Prepare all nested functions
    def press_enter_on_active_dialog():
        active_widget = app.activeModalWidget()
        QTest.keyClick(active_widget, Qt.Key_Enter)

    def press_escape_on_active_dialog():
        active_widget = app.activeModalWidget()
        QTest.keyClick(active_widget, Qt.Key_Escape)

    def press_add_button_on_active_dialog():
        active_widget = app.activeModalWidget()
        QTest.mouseClick(active_widget.button_ok, Qt.LeftButton)

    def press_cancel_button_on_active_dialog():
        active_widget = app.activeModalWidget()
        QTest.mouseClick(active_widget.button_cancel, Qt.LeftButton)

    # Perform tests
    dialog = dialog_class()
    QTimer.singleShot(0, press_enter_on_active_dialog)
    dialogcode = dialog.exec_()
    testcase.assertEqual(dialogcode, QDialog.DialogCode.Accepted)

    dialog = dialog_class()
    QTimer.singleShot(0, press_escape_on_active_dialog)
    dialogcode = dialog.exec_()
    testcase.assertEqual(dialogcode, QDialog.DialogCode.Rejected)

    dialog = dialog_class()
    QTimer.singleShot(0, press_add_button_on_active_dialog)
    dialogcode = dialog.exec_()
    testcase.assertEqual(dialogcode, QDialog.DialogCode.Accepted)

    dialog = dialog_class()
    QTimer.singleShot(0, press_cancel_button_on_active_dialog)
    dialogcode = dialog.exec_()
    testcase.assertEqual(dialogcode, QDialog.DialogCode.Rejected)


class TestAcceptRejectDialog(unittest.TestCase):
    """Testcase for AcceptRejectDialog class."""

    def test_dialog_codes(self):
        """Test dialog codes using helper function."""
        dialog_code_test_helper(self, AcceptRejectDialog)
