"""Test module that looks for differences in SOI modules across resolutions.

This was written as a regression test for #125. Ideally it should also test the
header of the SOI, but this is much more complicated to test for..

## Important

This test can not test across different resolutions by itself, this is up to
the user. Our pipeline will test across a couple of different resolutions.
"""
import unittest
from PySide2.QtWidgets import QApplication
from PySide2 import QtGui
from PySide2.QtCore import QTimer, Qt, QSysInfo
from PySide2.QtTest import QTest
from PySide2.QtGui import QGuiApplication
from soitool.soi import SOI
from soitool.modules.code_table_base import CodeTableSettings
from soitool.modules.module_authentication_board import (
    AuthenticationBoardModule,
)
from soitool.modules.module_subtractorcodes import SubtractorcodesModule
from soitool.modules.module_predefined_codes import PredefinedCodesModule
from soitool.modules.module_code_phrase import CodePhraseModule
from soitool.new_module_dialog import MODULE_CHOICES
from soitool.soi_workspace_widget import DATABASE_MODULES
from soitool.database import Database

# The error being ignored here is pylint telling us that 'test' is a standard
# module, so the import should be placed further up. In our case we have an
# actual custom module called 'test', so pylint is confused.
# pylint: disable=C0411
from test.test_database import TESTDBPATH


if isinstance(QtGui.qApp, type(None)):
    app = QApplication([])
else:
    app = QtGui.qApp

# Modules with a popup as part of their __init__
POPUP_MODULES = [
    AuthenticationBoardModule,
    SubtractorcodesModule,
    PredefinedCodesModule,
]


def screen_information():
    """Get string with information about the screen.

    Returns
    -------
    str
        String with screen information.
    """
    screen_string = ""
    screen = QGuiApplication.primaryScreen()
    screen_string += "screen.size() -> " + str(screen.size()) + "\n"
    screen_string += (
        "screen.physicalDotsPerInchX() -> "
        + str(screen.physicalDotsPerInchX())
        + "\n"
    )
    screen_string += (
        "screen.physicalDotsPerInchY() -> "
        + str(screen.physicalDotsPerInchY())
        + "\n"
    )
    screen_string += (
        "screen.logicalDotsPerInchX() -> "
        + str(screen.logicalDotsPerInchX())
        + "\n"
    )
    screen_string += (
        "screen.logicalDotsPerInchY() -> "
        + str(screen.logicalDotsPerInchY())
        + "\n"
    )
    screen_string += (
        "screen.devicePixelRatio() -> " + str(screen.devicePixelRatio()) + "\n"
    )
    return screen_string


class TestModulesAcrossResolutions(unittest.TestCase):
    """TestCase for modules across resolutions."""

    @unittest.skipUnless(
        QSysInfo.productType() == "windows",
        "Test currently only able to target Windows",
    )
    def test_add_all_modules(self):
        """Add all modules, reorganize, and assert result.

        Expected result was gotten by simply running reorganize and noting down
        the results. We should get the same result across different
        resolutions.

        Modules added to SOI must be the same every time the program is ran.

        Test only works on Windows, as the expected result is based on the
        Windows look-and-feel.

        NOTE: This test needs to be updated when a new module is added,
        deleted, or changed. It will fail until it is updated.
        """
        expected_result = [
            {"x": 0, "y": 0, "page": 1, "name": "PredefinedCodesModule"},
            {"x": 736, "y": 0, "page": 1, "name": "AuthenticationBoardModule"},
            {"x": 736, "y": 342, "page": 1, "name": "FrequencyTableModule"},
            {"x": 1876, "y": 0, "page": 1, "name": "SubtractorcodesModule"},
            {"x": 1320, "y": 0, "page": 1, "name": "PhonebookModule"},
            {"x": 1602, "y": 0, "page": 1, "name": "CodePhraseModule"},
            {"x": 1320, "y": 112, "page": 1, "name": "FreeTextModule"},
            {"x": 1430, "y": 112, "page": 1, "name": "TableModule"},
        ]

        # For use with modules that require a database
        database = Database(db_path=TESTDBPATH)

        def press_enter():
            active_widget = app.activeModalWidget()

            # AuthenticationBoardModule needs special treatment because the
            # title length can vary
            if isinstance(active_widget, CodeTableSettings):
                # Triple click to select existing text for overwrite
                QTest.mouseDClick(active_widget.edit_headline, Qt.LeftButton)
                QTest.mouseClick(active_widget.edit_headline, Qt.LeftButton)
                # Need to overwrite text because title otherwise contains
                # unpredictable text
                QTest.keyClicks(active_widget.edit_headline, "Title")
            QTest.keyClick(active_widget, Qt.Key_Enter)

        soi = SOI()

        for module in MODULE_CHOICES:
            # If we're adding one of the modules with a popup as part of it's
            # constructor we need to singleShot pressing enter to close it
            if module in POPUP_MODULES:
                QTimer.singleShot(0, press_enter)

            if module in DATABASE_MODULES:
                if module == CodePhraseModule:
                    # CodePhraseModule needs a category injected in it's init.
                    # Choosing "Tresort" because it contains words short enough
                    # to not expand the module beyond default width
                    module_instance = module(
                        database=database, category="Tresort"
                    )
                else:
                    module_instance = module(database=database)
            else:
                module_instance = module()

            soi.add_module(module.__name__, module_instance)

        soi.reorganize()

        self.assertEqual(
            expected_result,
            [module["meta"] for module in soi.modules],
            "All modules added to SOI and calling reorganize should result in "
            "expected 'meta' information. Screen is: \n"
            + screen_information(),
        )
